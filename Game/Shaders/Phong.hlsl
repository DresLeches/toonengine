#include "Constants.hlsl"

struct VIn
{
	float3 position : POSITION0;
	float3 normal : NORMAL0;
	float2 uv : TEXCOORD0;
};

struct VOut
{
	float4 position : SV_POSITION;
	float4 worldPos : POSITION0;
	float4 normal : NORMAL0;
	float2 uv : TEXCOORD0;
};

VOut VS(VIn vIn)
{
	VOut output;

	output.position = mul(float4(vIn.position, 1.0), c_modelToWorld);
	output.worldPos = output.position;
	output.position = mul(output.position, c_viewProj);
	output.normal = mul(float4(vIn.normal, 0.0), c_modelToWorld);

	output.uv = vIn.uv;
	return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);
	float3 norm = normalize(pIn.normal.xyz);
	float3 view = normalize(c_cameraPosition - pIn.worldPos.xyz);
	float3 phong = c_ambient;

	for (int i = 0; i < MAX_POINT_LIGHTS; i++) {
		if (c_pointLight[i].isEnabled) {
			float3 light = c_pointLight[i].position - pIn.worldPos.xyz;
			float dist = length(light);
			if (dist > 0.0) {
				float falloff = smoothstep(c_pointLight[i].outerRadius, c_pointLight[i].innerRadius, dist);
				light = light / dist;

				float diff = dot(light, norm);
				diff = max(diff, 0.0);
				phong = phong + falloff * c_pointLight[i].diffuseColor * diff;

				float3 ref = -reflect(light, norm);
				ref = normalize(ref);
				float spec = pow(dot(ref, view), c_pointLight[i].specularPower);
				spec = max(spec, 0.0);
				phong = phong + falloff * c_pointLight[i].specularColor * spec;
			}
		}
	}
	diffuse = diffuse * float4(phong, 1.0);
	return diffuse;
}