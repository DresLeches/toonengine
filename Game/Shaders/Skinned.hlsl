#include "Constants.hlsl"

#define PHONG 0
#define TOON 1

struct VIn
{
	float3 position : POSITION0;
	float3 normal : NORMAL0;
	uint4 bones : BONES0;
	float4 weight : WEIGHTS0;
	float2 uv : TEXCOORD0;
};

struct VOut
{
	float4 position : SV_POSITION;
	float4 worldPos : POSITION0;
	float4 normal : NORMAL0;
	float2 uv : TEXCOORD0;
};

float4 skinned(float4 position, uint4 bones, float4 weight)
{
	float4 result = weight.x * mul(position, c_skinMatrix[bones.x]);
	result = result + weight.y * mul(position, c_skinMatrix[bones.y]);
	result = result + weight.z * mul(position, c_skinMatrix[bones.z]);
	result = result + weight.w * mul(position, c_skinMatrix[bones.w]);
	return result;
}

VOut VS(VIn vIn)
{
	VOut output;
	float4 skinnedPos = skinned(float4(vIn.position, 1.0), vIn.bones, vIn.weight);
	float4 skinnedNorm = skinned(float4(vIn.normal, 0.0), vIn.bones, vIn.weight);

	output.position = mul(skinnedPos, c_modelToWorld);
	output.worldPos = output.position;
	output.position = mul(output.position, c_viewProj);
	output.normal = mul(skinnedNorm, c_modelToWorld);

	output.uv = vIn.uv;
	return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);
	float3 norm = normalize(pIn.normal.xyz);
	float3 view = normalize(c_cameraPosition - pIn.worldPos.xyz);
	float3 cel = c_ambient;

	for (int i = 0; i < MAX_POINT_LIGHTS; i++) 
	{
		if (c_pointLight[i].isEnabled) 
		{
			float3 light = c_pointLight[i].position - pIn.worldPos.xyz;
			float dist = length(light);
			if (dist > 0.0f && dist <= c_pointLight[i].outerRadius) 
			{
			   	light = light / dist;

				float diff = dot(light, norm);
				diff = max(diff, 0.0f);

				float celColor;
				if (diff > 0.90f) 
				{
					celColor = 0.90f;
				}
				else if (diff > 0.45f) 
				{
					celColor = 0.45f;
				}
				else 
				{
				   celColor = 0.05f;
				}
				cel = cel + c_pointLight[i].diffuseColor * celColor;
			}
		}
	}
	diffuse = diffuse * float4(cel, 1.0f);
	return diffuse;
}