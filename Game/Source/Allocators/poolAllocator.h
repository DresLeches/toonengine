#pragma once
#include <string>

template<class T> class PoolAllocator {
public:

    // Each poolblock is about 1024KB
    union PoolBlock {
		PoolBlock() {}
		~PoolBlock() {}

        T block[16];
        PoolBlock* pNext;
    };

	PoolAllocator();
	~PoolAllocator();

	void Initialize(int numBlocks);
	void Deinitialize();
	T* Allocate();
	void Deallocate(T* block);

private:
    PoolBlock* mBlocks;
    PoolBlock* firstBlock;
    int maxNumBlocks;
};

template<class T>
PoolAllocator<T>::PoolAllocator()
{
}

template<class T>
PoolAllocator<T>::~PoolAllocator() 
{
}

template<class T>
void PoolAllocator<T>::Initialize(int numBlocks) {
    maxNumBlocks = numBlocks;
	mBlocks = new PoolBlock[maxNumBlocks];

    firstBlock = mBlocks;
    for (int i = 0; i < maxNumBlocks-1; i++) {
        mBlocks[i].pNext = &mBlocks[i+1];
    }
    mBlocks[maxNumBlocks - 1].pNext = nullptr;
}

template<class T>
void PoolAllocator<T>::Deinitialize() {
	delete[] mBlocks;
}

template<class T>
T* PoolAllocator<T>::Allocate() {
    if (firstBlock != nullptr) {
        PoolBlock* allocBlock = firstBlock;
        firstBlock = firstBlock->pNext;
        allocBlock->pNext = nullptr;
		ZeroMemory(allocBlock->block, sizeof(allocBlock->block));
        return allocBlock->block;
    }
    else {
        return nullptr;
    }
}

template<class T>
void PoolAllocator<T>::Deallocate(T* block) {
    PoolBlock* temp = (firstBlock != nullptr) ? firstBlock->pNext : nullptr;
    firstBlock = reinterpret_cast<PoolBlock*>(block);
    firstBlock->pNext = temp;
}

