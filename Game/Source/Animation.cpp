#include "stdafx.h"
#include "Animation.h"
#include "game.h"
#include "Skeleton.h"
#include "BoneTransform.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include "jsonUtil.h"
#include <fstream>
#include <sstream>

Animation::Animation(Game* game)
:mGame(game)
,currFrame(0)
{
}

Animation::~Animation() {
    
}

Animation* Animation::StaticLoad(const WCHAR* fileName, Game* pGame) 
{
    Animation* anim = new Animation(pGame);

    if (false == anim->Load(fileName)) 
	{
        delete anim;
        return nullptr;
    }
    return anim;
}

void Animation::GetGlobalPoseAtTime(std::vector<Matrix4>& outPoses,
                             const Skeleton *inSkeleton,
                             float inTime ) const
{
	int currFrame = (int)(inTime * (mNumFrames - 1) / mLength);
	float interp = inTime;
	float interval = mLength / (mNumFrames - 1);
	if (interval < inTime) 
	{
		int currIntervalNum = (int)(inTime / interval);
		interp = inTime - (float)(currIntervalNum)* interval; // Get the second between the interval
	}
	interp = interp * ((mNumFrames - 1) / mLength);
	int nextFrame = currFrame + 1;

	for (uint32_t i = 0; i < mNumBones; i++) 
	{
		std::vector<BoneTransform> boneTrans = mTracks[i];
		Skeleton::Bone currBone = inSkeleton->GetBone(i);
		Matrix4 pose;
		if (!boneTrans.empty()) 
		{
			BoneTransform bt = BoneTransform::Interpolate(mTracks[i][currFrame],
				mTracks[i][nextFrame],
				interp);
			pose = bt.ToMatrix();
		}

		if (currBone.mParent != -1) 
		{
			pose *= outPoses[currBone.mParent];
		}
		outPoses.push_back(pose);
	}
}

bool Animation::Load(const WCHAR* fileName) 
{
    std::ifstream file(fileName);
    if (!file.is_open()) 
	{
        return false;
    }

    std::stringstream ss;
    ss << file.rdbuf();
    std::string content = ss.str();
    rapidjson::StringStream jsonStr(content.c_str());
    rapidjson::Document doc;
    doc.ParseStream(jsonStr);

    if (!doc.IsObject()) 
	{
        return false;
    }

    std::string str = doc["metadata"]["type"].GetString();
    int ver = doc["metadata"]["version"].GetInt();

    // Check metadata
    if (!doc["metadata"].IsObject() ||
        str != "itpanim" ||
        ver != 2)
    {
        return false;
    }

    // Parse each sequence 
    const rapidjson::Value& seq = doc["sequence"];
    if (!seq.IsObject()) {
        return false; 
    }

    int frames = 0;
    float length = 0;
    int bonecount = 0;
    frames = seq["frames"].GetInt();
    length = seq["length"].GetDouble();
    bonecount = seq["bonecount"].GetInt();
    mNumFrames = frames;
    mLength = length;
    mNumBones = bonecount;
    mTracks.resize(bonecount);
    
    // Iterate through the tracks
    const rapidjson::Value& tracks = seq["tracks"];

    if (!tracks.IsArray()) 
	{
        return false;
    }

    for (rapidjson::SizeType i = 0; i < tracks.Size(); i++) 
	{
        const rapidjson::Value& bone = tracks[i]["bone"];
        const rapidjson::Value& transform = tracks[i]["transforms"];

        if (!bone.IsNumber() ||
            !transform.IsArray())
        {
            return false;
        }

        int boneInd = bone.GetInt();
        std::vector<BoneTransform> boneTrans;
        for (rapidjson::SizeType j = 0; j < transform.Size(); j++) 
		{

            BoneTransform temp;
            GetQuaternionFromJSON(transform[j], "rot", temp.mRotation);
            GetVectorFromJSON(transform[j], "trans", temp.mTranslation);
            boneTrans.push_back(temp);
        }
        mTracks[boneInd] = boneTrans;
    }
    return true;
}




