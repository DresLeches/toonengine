#pragma once
#include "engineMath.h"
#include <vector>

class Game;
struct BoneTransform;
class Skeleton;

class Animation {
public:
    Animation(Game* game);
    ~Animation();

    static Animation* StaticLoad(const WCHAR* fileName, Game* pGame);
    bool Load(const WCHAR* fileName);

    uint32_t GetNumBones() const { return mNumBones; }
    uint32_t GetNumFrames() const { return mNumFrames; }
    float GetLength() const { return mLength; }
    void GetGlobalPoseAtTime(std::vector<Matrix4>& outPoses,
                             const Skeleton *inSkeleton,
                             float inTime ) const;

private:
    Game* mGame;
    uint32_t mNumBones;
    uint32_t mNumFrames;
    float mLength;
    std::vector<std::vector<BoneTransform>> mTracks;
    int currFrame;
};
