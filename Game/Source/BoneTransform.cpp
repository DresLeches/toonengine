#include "stdafx.h"
#include "BoneTransform.h"

Matrix4 BoneTransform::ToMatrix() const {
	Matrix4 rot = Matrix4::CreateFromQuaternion(mRotation);
	Matrix4 transform = Matrix4::CreateTranslation(mTranslation);

	return rot * transform;
}

BoneTransform BoneTransform::Interpolate(const BoneTransform& a,
                                         const BoneTransform& b,
                                         float f)
{
    BoneTransform res;

    res.mRotation = Slerp(a.mRotation, b.mRotation, f);
    res.mTranslation = Lerp(a.mTranslation, b.mTranslation, f);
    return res;
}
