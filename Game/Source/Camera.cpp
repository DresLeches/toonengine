#include "stdafx.h"
#include "Camera.h"

Camera::Camera(Graphics* graphics)
:mGraphics(graphics)
{
    mProj = Matrix4::CreateRotationY(-Math::PiOver2)
          * Matrix4::CreateRotationZ(-Math::PiOver2)
		  * Matrix4::CreatePerspectiveFOV(Math::ToRadians(70.0f),
            mGraphics->GetScreenWidth(), mGraphics->GetScreenHeight(),
            25.0f, 10000.0f);

    mCameraBuff = mGraphics->CreateGraphicsBuffer(&mCameraData, sizeof(mCameraData), 
                                                  D3D11_BIND_CONSTANT_BUFFER,
                                                  D3D11_CPU_ACCESS_WRITE,
                                                  D3D11_USAGE_DYNAMIC);

}

Camera::~Camera() {
    mCameraBuff->Release();
}

void Camera::SetActive() {

    ID3D11DeviceContext* devCon = mGraphics->GetDeviceContext();

    mCameraData.c_viewProj = mView * mProj;
    Matrix4 tempMat(mView);
    tempMat.Invert();
    mCameraData.c_cameraPosition = Vector3(tempMat.mat[3][0], tempMat.mat[3][1], tempMat.mat[3][2]);
    mGraphics->UploadBuffer(mCameraBuff, &mCameraData, sizeof(mCameraData));
    devCon->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_CAMERA, 1, &mCameraBuff);
    devCon->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_CAMERA, 1, &mCameraBuff);
}
