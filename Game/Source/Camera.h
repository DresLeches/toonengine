#pragma once
#include "stdafx.h"
#include "engineMath.h"
#include "Graphics.h"

class Camera {
public:

    Camera(Graphics* graphics);
    virtual ~Camera();
    void SetActive();
    void SetViewMatrix(Matrix4& newView) { mView = newView; }

    struct PerCameraConstants {
        Matrix4 c_viewProj;
        Vector3 c_cameraPosition;
        float x;    // For memory alignment
    };
    PerCameraConstants mCameraData;

protected:
    Graphics* mGraphics;
    ID3D11Buffer* mCameraBuff;

    // View and Projection matrix
    Matrix4 mView;
    Matrix4 mProj;
};
