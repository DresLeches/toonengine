#include "stdafx.h"
#include "BoundingVolumes.h"
#include "engineMath.h"

// Axis-Aligned Bounding Box
AABB::AABB()
    :mMin(Vector3::Zero)
    ,mMax(Vector3::Zero)
{
}

AABB::AABB(Vector3 min, Vector3 max)
    :mMin(min)
    ,mMax(max)
{
}

AABB::~AABB()
{
}

// Object Bounding Box
OBB::OBB()
    :mCenter(Vector3::Zero)
{
    for (int i = 0; i < 3; i++)
    {
        mAxis[i] = Vector3::Zero;
        mHalfWidth[i] = 0;
    }
}

OBB::OBB(Vector3 center, Vector3 axis[3], float halfWidth[3])
    :mCenter(center)
//    ,mAxis(axis)
     //   ,mHalfWidth(halfWidth)
{
    //mAxis = axis;
    //mHalfWidth = halfWidth;
}

OBB::~OBB()
{
}

// Sphere
BoundingSphere::BoundingSphere()
    :mCenter(Vector3::Zero)
    ,mRadius(0.0f)
{
}

BoundingSphere::BoundingSphere(Vector3 center, float radius)
    :mCenter(center)
    ,mRadius(radius)
{
}

BoundingSphere::~BoundingSphere()
{
}
