#pragma once
#include "engineMath.h"

/******************************************************
 *                                                    *
 *  BoundingVolume: Structs for the bounding volumes  *
 *                                                    *
 ******************************************************/

struct AABB {
    AABB();
    AABB(Vector3 min, Vector3 max);
    ~AABB();

    Vector3 mMin;
    Vector3 mMax;
};

struct OBB {
    OBB();
    OBB(Vector3 center, Vector3 axis[3], float halfWidth[3]);
    ~OBB();

    Vector3 mCenter;
    Vector3 mAxis[3];
    float mHalfWidth[3];
};

struct BoundingSphere {
    BoundingSphere();
	BoundingSphere(Vector3 center, float radius);
    ~BoundingSphere();

    Vector3 mCenter;
    float mRadius;
};

