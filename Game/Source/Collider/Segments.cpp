#include "stdafx.h"
#include "Collider/Segments.h"
#include "engineMath.h"

Ray::Ray() 
	:mFrom(Vector3::Zero)
	,mTo(Vector3::Zero)
{
}

Ray::Ray(Vector3 from, Vector3 to)
	:mFrom(from)
	,mTo(to)
{
}

Ray::~Ray()
{
}