#pragma once
#include "engineMath.h"

/*************************************************************
 *                                                           *
 *  Segements: Structs for the colision detection algorithms * 
 *             for lines, segments, and ray                  *
 *                                                           *
 *************************************************************/

struct Ray {
    Ray();
    Ray(Vector3 from, Vector3 to);
    ~Ray();

    Vector3 mFrom;
    Vector3 mTo;
};

