#include "stdafx.h"
#include "Character.h"
#include "RenderObj.h"
#include "RenderObjs/SkinnedObj.h"
#include "Skeleton.h"
#include "Animation.h"
#include "game.h"
#include "jsonUtil.h"
#include "stringUtil.h"
#include "ThreadJobs/AnimationJob.h"
#include "JobManager.h"
#include "GameDebug.h"

Character::Character(SkinnedObj* pObj)
:Component(pObj)
,mSkinnedObj(pObj)
,mCurrentAnim(nullptr)
,mAnimationTime(0.0f)
{
}

Character::~Character() 
{    
}

void Character::LoadProperties(const rapidjson::Value& properties) 
{
	std::wstring skName;
	Game* game = mSkinnedObj->GetGame();
	GetWStringFromJSON(properties, "skeleton", skName);
	mSkeleton = game->LoadSkeleton(skName);

	const rapidjson::Value& anim = properties["animations"];
	for (rapidjson::SizeType j = 0; j < anim.Size(); j++) 
	{
		std::string animType = anim[j][0].GetString();
		std::string temp = anim[j][1].GetString();
		std::wstring animStr;
		StringUtil::String2WString(animStr, temp);

		Animation* animation = game->LoadAnimation(animStr);
		mAnims[animType] = animation;
	}

}

bool Character::SetAnim(const std::string& animName) 
{
    auto it = mAnims.find(animName);
    if (it != mAnims.end()) 
	{
        mCurrentAnim = it->second;
        mAnimationTime = 0.0f;
        return true;
    }
    return false;
}

void Character::UpdateAnim(float deltaTime) 
{
    if (mCurrentAnim != nullptr) 
	{
        mAnimationTime += deltaTime;
        if (mAnimationTime > mCurrentAnim->GetLength()) 
		{
			int numLoop = (int)(mAnimationTime / mCurrentAnim->GetLength());
			mAnimationTime = mAnimationTime - (float)(numLoop * mCurrentAnim->GetLength());
        }

		JobManager::GetJobManager()->AddJob(new AnimationJob(this));
    }
}

void Character::Update(float deltaTime) 
{
	// Test the character animation by putting
	// it in slow motion
#if SLOW_MOTION
	deltaTime = 0.005f;
#endif
    if (mCurrentAnim == nullptr) 
	{
#if ROBOT_RUN
		SetAnim("run");
#else
        SetAnim("idle");
#endif
    }
    UpdateAnim(deltaTime);
}
