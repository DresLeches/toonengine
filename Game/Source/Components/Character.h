#pragma once
#include "stdafx.h"
#include "Component.h"
#include "ThreadJobs/AnimationJob.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"

class SkinnedObj;
class Skeleton;
class Animation;

class Character : public Component {
public: 
    Character(SkinnedObj* pObj);
    ~Character();

    void LoadProperties(const rapidjson::Value& properties);
    bool SetAnim(const std::string& animName);
    void UpdateAnim(float deltaTime);
    void Update(float deltaTime);
	virtual void ChangeState() { }

private:
    SkinnedObj* mSkinnedObj;
    Skeleton* mSkeleton;
    std::unordered_map<std::string, const Animation*> mAnims;
    const Animation* mCurrentAnim;
    float mAnimationTime;

    friend class AnimationJob;
    friend class Player;
};

