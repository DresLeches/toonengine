#include "stdafx.h"
#include "CollisionComponent.h"
#include "Collider/BoundingVolumes.h"
#include "RenderObj.h"
#include "jsonUtil.h"

CollisionComponent::CollisionComponent(RenderObj* pObj)
    :Component(pObj)
{
}

CollisionComponent::~CollisionComponent()
{
}

void CollisionComponent::LoadProperties(const rapidjson::Value& properties)
{
    GetVectorFromJSON(properties, "min", mAABB.mMin);
	GetVectorFromJSON(properties, "max", mAABB.mMax);   
}

// Compute the AABB-AABB
AABB CollisionComponent::GetAABB() const
{
    AABB aabb = mAABB;
	aabb.mMin = mAABB.mMin * mObj->mObjectData.c_modelToWorld.GetScale().x;
	aabb.mMax = mAABB.mMax * mObj->mObjectData.c_modelToWorld.GetScale().x;
	
	aabb.mMin = aabb.mMin + mObj->mObjectData.c_modelToWorld.GetTranslation();
	aabb.mMax = aabb.mMax + mObj->mObjectData.c_modelToWorld.GetTranslation();
	return aabb;
}



