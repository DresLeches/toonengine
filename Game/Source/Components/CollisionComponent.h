#pragma once
#include "Component.h"
#include "Physics.h"
#include "Collider/BoundingVolumes.h"

class RenderObj;

class CollisionComponent : public Component {
public:
    CollisionComponent(RenderObj* pObj);
    ~CollisionComponent();

    void LoadProperties(const rapidjson::Value& properties);
    AABB GetAABB() const;
    
private:
    AABB mAABB;
};
