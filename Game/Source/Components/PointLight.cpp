#include "stdafx.h"
#include "PointLight.h"
#include "RenderObj.h"
#include "jsonUtil.h"
#include "LightingData.h"

PointLight::PointLight(RenderObj* pObj)
:Component(pObj)
{
    mGame = mObj->GetGame();
    mLight = mGame->AllocateLight();
}
PointLight::~PointLight() 
{
    mGame->FreeLight(mLight);
}
void PointLight::LoadProperties(const rapidjson::Value& properties) 
{
	Vector3 diffuseColor;
	Vector3 specularColor;
	float specularPower;
	float innerRadius;
	float outerRadius;
	GetVectorFromJSON(properties, "diffuseColor", diffuseColor);
	GetVectorFromJSON(properties, "specularColor", specularColor);
	GetFloatFromJSON(properties, "specularPower", specularPower);
	GetFloatFromJSON(properties, "innerRadius", innerRadius);
	GetFloatFromJSON(properties, "outerRadius", outerRadius);

	mLight->mDiffuseColor = diffuseColor;
	mLight->mSpecularColor = specularColor;
	mLight->mSpecularPower = specularPower;
	mLight->mInnerRadius = innerRadius;
	mLight->mOuterRadius = outerRadius;

	Matrix4 tempModelToWorld = mObj->mObjectData.c_modelToWorld;
	Vector3 pos(tempModelToWorld.mat[3][0], tempModelToWorld.mat[3][1], tempModelToWorld.mat[3][2]);
	mLight->mPosition = pos;

}
