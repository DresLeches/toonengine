#pragma once
#include "Component.h"
#include "game.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"

class RenderObj;
struct PointLightData;

class PointLight : public Component {
public:
    PointLight(RenderObj* pObj);
    ~PointLight();
    void LoadProperties(const rapidjson::Value& properties);

private:
    PointLightData* mLight;
    Game* mGame;
};
