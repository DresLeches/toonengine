#include "stdafx.h"
#include "SimpleRotate.h"
#include "RenderObj.h"
#include "engineMath.h"
#include "jsonUtil.h"

SimpleRotate::SimpleRotate(RenderObj* pObj)
:Component(pObj)
,mRotSpeed(0.0f)
,mCurrAngle(0.0f)
{
}

SimpleRotate::~SimpleRotate()
{
}

void SimpleRotate::LoadProperties(const rapidjson::Value& properties)
{
    GetFloatFromJSON(properties, "speed", mRotSpeed);
}

void SimpleRotate::Update(float deltaTime)
{
    mCurrAngle += mRotSpeed * deltaTime;
    Matrix4 objMat = mObj->mObjectData.c_modelToWorld;
    Matrix4 scaleMat = objMat.CreateScale(objMat.GetScale());
    Matrix4 rotMat = objMat.CreateRotationZ(mCurrAngle);
    Matrix4 transMat = objMat.CreateTranslation(objMat.GetTranslation());

    mObj->mObjectData.c_modelToWorld = scaleMat * rotMat * transMat;
}
