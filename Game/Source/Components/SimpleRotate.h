#pragma once
#include "Component.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"

class RenderObj;

class SimpleRotate : public Component {
public:

    SimpleRotate(RenderObj* pObj);
    ~SimpleRotate();

    void LoadProperties(const rapidjson::Value& properties);
    void Update(float deltaTime);
private:
	float mCurrAngle;
    float mRotSpeed;
};

