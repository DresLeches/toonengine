#include "stdafx.h"
#include "camera.h"
#include "components\followCam.h"
#include "game.h"
#include "jsonUtil.h"
#include "renderObj.h"
#include "stringUtil.h"


FollowCam::FollowCam(RenderObj *pObj)
    : Component(pObj)
{
}

void FollowCam::LoadProperties(const rapidjson::Value& properties)
{
    Component::LoadProperties(properties);
    GetVectorFromJSON(properties, "offset", mOffset);
}

void FollowCam::Update(float deltaTime)
{
    Camera* pCamera = mObj->GetGame()->GetCamera();

	// M = SRT
    Matrix4 mat = Matrix4::CreateTranslation(mObj->mObjectData.c_modelToWorld.GetTranslation() + mOffset);
    mat.Invert();
    pCamera->SetViewMatrix(mat);
}