#pragma once

// This contains all the debugging macros used throughout the program

// GENERAL DEBUG MACROS
#define MEM_LEAK 0

// ANIMATION DEBUG MACROS
#define SLOW_MOTION 0
#define ROBOT_ARMY 0
#define ROBOT_RUN 0

// PERFORMANCE TEST MACROS
#define HASH_COLL 0
#define TOGGLE_PROFILER 1

// TOGGLE BETWEEN THE ASSETCACHE
#define NEW_ASSET_CACHE 0


