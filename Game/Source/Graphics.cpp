#include "stdafx.h"
#include "Graphics.h"
#include "Shader.h"
#include "engineMath.h"

#pragma comment (lib, "d3d11.lib") 

Graphics::Graphics()
    : mScreenWidth(0)
    , mScreenHeight(0)
	, mSwapchain(nullptr)
	, mDev(nullptr)
	, mDevcon(nullptr)
{
}

Graphics::~Graphics()
{
}

void Graphics::InitD3D(HWND hWnd, float width, float height)
{
    mScreenWidth = width;
    mScreenHeight = height;

    // Swap Chain 
	DXGI_SWAP_CHAIN_DESC scd;
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));    
	scd.BufferCount = 1;	// one back buffer     
	scd.BufferDesc.Width = (UINT)mScreenWidth;     
	scd.BufferDesc.Height = (UINT)mScreenHeight;    
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // use 32-bit color     
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;   // how swap chain is to be used     
	scd.OutputWindow = hWnd;                             // the window to be used     
	scd.SampleDesc.Count = 1;                            // how many multisamples     
	scd.Windowed = TRUE;                                 // windowed/full-screen mode

	HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL,         
					D3D_DRIVER_TYPE_HARDWARE,         
					NULL,         
					D3D11_CREATE_DEVICE_DEBUG,         
					NULL,         
					NULL,         
					D3D11_SDK_VERSION,         
					&scd,         
					&mSwapchain,         
					&mDev,         
					NULL,         
					&mDevcon);

    // Viewport
	D3D11_VIEWPORT vp;
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	mDevcon->RSSetViewports(1, &vp);

    // Sampler
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(sampDesc));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    hr = mDev->CreateSamplerState(&sampDesc, &mDefaultSampler);
    SetActiveSampler(0, mDefaultSampler);

    // Rasterizer
    D3D11_RASTERIZER_DESC rastDesc;
    ZeroMemory(&rastDesc, sizeof(rastDesc));
    rastDesc.FillMode = D3D11_FILL_SOLID;
    rastDesc.CullMode = D3D11_CULL_BACK;
    rastDesc.FrontCounterClockwise = true;

    //CreateRasterizerState
    ID3D11RasterizerState* rastState;
    hr = mDev->CreateRasterizerState(&rastDesc, &rastState);
    DbgAssert(hr == S_OK, "Rasterizer state failed to get created");
    mDevcon->RSSetState(rastState);
    rastState->Release();

    // Back Buffer
	ID3D11Texture2D *pBackBuffer;     
	hr = mSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);     
	DbgAssert(hr == S_OK, "Something wrong with your back buffer"); 
	hr = mDev->CreateRenderTargetView(pBackBuffer, nullptr, &mBackBuffer);
	pBackBuffer->Release();

    bool success = CreateDepthStencil((int) width, (int) height, &mDepthStencilTexture, &mDepthStencilView);
    DbgAssert(success == true, "Something went wrong with the depth stencil");
    mDepthState = CreateDepthStencilState(true, D3D11_COMPARISON_LESS_EQUAL);

    // Create the back buffer and stencil state
    mDevcon->OMSetRenderTargets(1, &mBackBuffer, mDepthStencilView);
	mDevcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	mDevcon->OMSetDepthStencilState(mDepthState, 0);
}

void Graphics::BeginFrame(const Color4 &clearColor)
{
    mDevcon->ClearRenderTargetView(mBackBuffer, (const float*)&clearColor);
    mDevcon->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Graphics::EndFrame()
{
	mSwapchain->Present(1, 0);
}

void Graphics::CleanD3D()
{
	mSwapchain->Release();
	mDev->Release();
	mDevcon->Release();
	mBackBuffer->Release();
    mDepthStencilTexture->Release();
    mDepthStencilView->Release();
    mDepthState->Release();
    mDefaultSampler->Release();
}

void Graphics::UploadBuffer(ID3D11Buffer* buffer, const void* data, size_t dataSize) {
	D3D11_MAPPED_SUBRESOURCE ms;
	mDevcon->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &ms);
	memcpy(ms.pData, data, dataSize);
	mDevcon->Unmap(buffer, 0);
}

ID3D11Buffer *Graphics::CreateGraphicsBuffer(const void *initialData, int inDataSize, 
											 D3D11_BIND_FLAG inBindFlags, 
											 D3D11_CPU_ACCESS_FLAG inCPUAccessFlags, 
											 D3D11_USAGE inUsage) 
{
	D3D11_BUFFER_DESC bd;
	ID3D11Buffer* buf;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.Usage = inUsage;
	bd.ByteWidth = inDataSize;
	bd.BindFlags = inBindFlags;
	bd.CPUAccessFlags = inCPUAccessFlags;
	mDev->CreateBuffer(&bd, nullptr, &buf);

	if (initialData != nullptr) {
		UploadBuffer(buf, initialData, inDataSize);
	}
	return buf;
}

bool Graphics::CreateDepthStencil(int inWidth, int inHeight,
                            ID3D11Texture2D **pDepthTexture,
                            ID3D11DepthStencilView **pDepthView)
{
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory(&descDepth, sizeof(descDepth));
    descDepth.Width = inWidth;
    descDepth.Height = inHeight;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    HRESULT hr = mDev->CreateTexture2D(&descDepth, nullptr, pDepthTexture);   
    DbgAssert(hr == S_OK, "Something wrong with your depth stencil");   

    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory(&descDSV, sizeof(descDSV));
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = mDev->CreateDepthStencilView(*pDepthTexture, &descDSV, pDepthView);
    DbgAssert(hr == S_OK, "Something wrong with your depth view");
    return (hr == S_OK);
}

ID3D11DepthStencilState* Graphics::CreateDepthStencilState(bool inDepthTestEnable,
                                                           D3D11_COMPARISON_FUNC inDepthComparisonFunction)
{
    D3D11_DEPTH_STENCIL_DESC dsDesc;
    dsDesc.DepthEnable = inDepthTestEnable;
    dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsDesc.DepthFunc = inDepthComparisonFunction;
    dsDesc.StencilEnable = false;
    dsDesc.StencilReadMask = 0xFF;
    dsDesc.StencilWriteMask = 0xFF;
    dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
    dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    ID3D11DepthStencilState* depthState = nullptr;
    HRESULT hr = mDev->CreateDepthStencilState(&dsDesc, &depthState);   
    DbgAssert(hr == S_OK, "Something wrong with your Stencil State");
    return depthState;
}

void Graphics::SetActiveTexture(int slot, ID3D11ShaderResourceView* pView) {
    mDevcon->PSSetShaderResources(slot, 1, &pView);
}

void Graphics::SetActiveSampler(int slot, ID3D11SamplerState* pSampler) {
    mDevcon->PSSetSamplers(slot, 1, &pSampler);
}
