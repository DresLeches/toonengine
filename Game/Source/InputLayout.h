#pragma once
#include "stdafx.h"

/***********************************************************
 * InputLayout.h
 *  
 * Contains the struct and input layout for the shaders to
 * use.
 **********************************************************/

struct VertexPosColor {
	Vector3 pos;
	Graphics::Color4 color;
};

struct VertexPosUV {
	Vector3 pos;
	Vector3 normal;
	Vector2 uv;
};

struct VertexPosSkinUV {
	Vector3 pos;
	Vector3 normal;
	uint8_t bones[4];
	uint8_t weights[4];
	Vector2 uv;
};

struct VertexPosUVTan {
	Vector3 pos;
	Vector3 normal;
	Vector3 tangent;
	Vector2 uv;
};

namespace InputLayout
{
    D3D11_INPUT_ELEMENT_DESC inputElem[] =  {     
        { 
            "POSITION", 0, 
            DXGI_FORMAT_R32G32B32_FLOAT, 0, 
            offsetof(VertexPosColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 
        },     
        { 
            "COLOR", 0, 
            DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 
            offsetof(VertexPosColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0 
        } 
    }; 


    D3D11_INPUT_ELEMENT_DESC inputMesh[] =  {     
        { 
            "POSITION", 0, 
            DXGI_FORMAT_R32G32B32_FLOAT, 0, 
            offsetof(VertexPosUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 
        },
        {
            "NORMAL", 0,
            DXGI_FORMAT_R32G32B32_FLOAT, 0,
            offsetof(VertexPosUV, normal), D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "TEXCOORD", 0,
            DXGI_FORMAT_R32G32_FLOAT, 0,
            offsetof(VertexPosUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0
        }
    };

    D3D11_INPUT_ELEMENT_DESC inputSkinnedMesh[] =  {     
        { 
            "POSITION", 0, 
            DXGI_FORMAT_R32G32B32_FLOAT, 0, 
            offsetof(VertexPosSkinUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 
        },
        {
            "NORMAL", 0,
            DXGI_FORMAT_R32G32B32_FLOAT, 0,
            offsetof(VertexPosSkinUV, normal), D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "BONES", 0,
            DXGI_FORMAT_R8G8B8A8_UINT, 0,
            offsetof(VertexPosSkinUV, bones), D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "WEIGHTS", 0,
            DXGI_FORMAT_R8G8B8A8_UNORM, 0,
            offsetof(VertexPosSkinUV, weights), D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "TEXCOORD", 0,
            DXGI_FORMAT_R32G32_FLOAT, 0,
            offsetof(VertexPosSkinUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0
        }
    };

	D3D11_INPUT_ELEMENT_DESC inputNormal[] = {
		{
			"POSITION", 0,
			DXGI_FORMAT_R32G32B32_FLOAT, 0,
			offsetof(VertexPosUVTan, pos), D3D11_INPUT_PER_VERTEX_DATA, 0
		},
		{
			"NORMAL", 0,
			DXGI_FORMAT_R32G32B32_FLOAT, 0,
			offsetof(VertexPosUVTan, normal), D3D11_INPUT_PER_VERTEX_DATA, 0
		},
		{
			"TANGENT", 0,
			DXGI_FORMAT_R32G32B32_FLOAT, 0,
			offsetof(VertexPosUVTan, tangent), D3D11_INPUT_PER_VERTEX_DATA, 0
		},
		{
			"TEXCOORD", 0,
			DXGI_FORMAT_R32G32_FLOAT, 0,
			offsetof(VertexPosUVTan, uv), D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
}


