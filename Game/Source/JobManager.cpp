#include "stdafx.h"
#include "JobManager.h"
#include "Job.h"

#define LOAD_STORE 0

static std::atomic<int> numJobs;

JobManager::JobManager()
{
}

JobManager::~JobManager() 
{
    while (!mJobList.empty()) 
	{
        Job* j = mJobList.front();
        mJobList.pop();
        delete j;
    }
}

JobManager* JobManager::GetJobManager() 
{
	static JobManager jb;
	return &jb;
}

void JobManager::Begin() {
    for (int i = 0; i < NUM_WORKERS; i++) 
	{
        workers[i].Begin();
    }
}

void JobManager::End() {
	Worker::SignalThreadAll();
    for (int i = 0; i < NUM_WORKERS; i++) 
	{
        workers[i].End();
    }
}

void JobManager::AddJob(Job* j) 
{
	mJobListLock.lock();
    numJobs++;
	mJobList.push(j);
	mJobListLock.unlock();
}

Job* JobManager::GetAvailableJob() 
{
	mJobListLock.lock();

    if (mJobList.empty()) 
	{
		mJobListLock.unlock();
        return nullptr;
    }
    Job* res = mJobList.front();
    mJobList.pop();
	mJobListLock.unlock();
    return res;
}

void JobManager::WorkerFinishedJob() 
{
    numJobs--;
}

void JobManager::WaitForJobs() 
{

    while (numJobs != 0) 
	{
        Sleep(1);
    }
}