#pragma once
#include "Worker.h"
#include <queue>
#include <mutex>
#include <atomic>

#define NUM_WORKERS 4

class Job;

class JobManager {
public:
    
	static JobManager* GetJobManager();
    
    void Begin();
    void End();
    void AddJob(Job* j);
    void WaitForJobs();

    // Functions to communicate between Job Manager and Workers
    Job* GetAvailableJob();
    void WorkerFinishedJob();
    
private:
	JobManager();
	~JobManager();

	Worker workers[NUM_WORKERS];
    std::queue<Job*> mJobList;
	std::mutex mJobListLock;
};
