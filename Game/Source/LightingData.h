#pragma once

#define MAX_POINT_LIGHTS 8

// Point light data struct
// Added padding for 16 byte alignment
struct PointLightData {
   Vector3 mDiffuseColor;
   float padding1;
   Vector3 mSpecularColor;
   float padding2;
   Vector3 mPosition;
   float mSpecularPower;
   float mInnerRadius;
   float mOuterRadius;
   bool mIsEnabled;
   bool padding3;
   bool padding4;
   bool padding5;
   float padding6;
};

struct LightingData {
    Vector3 c_ambient;
	float padding;		// Padding for memory alignment
	PointLightData c_pointLight[MAX_POINT_LIGHTS];
};
