#include "stdafx.h"
#include "Physics.h"
#include "engineMath.h"
#include "Components/CollisionComponent.h"
#include "Collider/BoundingVolumes.h"
#include "Collider/Segments.h"

// AABB-AABB Intersection Test
bool Physics::Intersect(const AABB& a, const AABB& b, AABB* pOverlap)
{
    // If one of these are true, then an intersection has not occurred
    if (a.mMin.x > b.mMax.x || a.mMax.x < b.mMin.x ||
		a.mMin.y > b.mMax.y || a.mMax.y < b.mMin.y ||
		a.mMin.z > b.mMax.z || a.mMax.z < b.mMin.z)
    {
		if (pOverlap != nullptr)
        {
			pOverlap->mMin = Vector3::One;
			pOverlap->mMax = Vector3::Zero;
		}
		return false;
	}

    if (pOverlap != nullptr)
    {
        pOverlap->mMin = Vector3(Math::Max(a.mMin.x, b.mMin.x), Math::Max(a.mMin.y, b.mMin.y), Math::Max(a.mMin.z, b.mMin.z));
        pOverlap->mMax = Vector3(Math::Min(a.mMax.x, b.mMax.x), Math::Min(a.mMax.y, b.mMax.y), Math::Min(a.mMax.z, b.mMax.z));
    }
    return true;
}

// OBB-OBB Intersection Test
bool Physics::Intersect(const OBB& a, const OBB& b, OBB* pOverlap)
{
    float ra = 0.0f;
    float rb = 0.0f;
    Matrix3 rot = Matrix3::Identity;
    Matrix3 subExp = Matrix3::Identity;
    
    // Compute the rotation matrix
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rot.mat[i][j] = Dot(a.mAxis[i], b.mAxis[j]);
        }
    }

    // Calculate the translation t
    Vector3 tempTrans = b.mCenter - a.mCenter;
    tempTrans = Vector3(Dot(tempTrans, a.mAxis[0]), Dot(tempTrans, a.mAxis[1]), Dot(tempTrans, a.mAxis[2]));
    float* trans = (float*) &tempTrans;

    // Compute common subexpressions
    // Counteract the error when two edges are parallel
    const int epsilon = 0.001f;   
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            subExp.mat[i][j] = abs(rot.mat[i][j]) + epsilon;
        }
    }

    // Test axis #1: L = A0, L = A1, L = A2
    for (int i = 0; i < 3; i++)
    {
        ra = a.mHalfWidth[i];
        rb = b.mHalfWidth[0] * subExp.mat[i][0] + b.mHalfWidth[1] * subExp.mat[i][1]
            + b.mHalfWidth[2] * subExp.mat[i][2];
        if (abs(trans[i]) > ra + rb)
        {
            return false;    
        }
    }
    
    // Text axis #2: L = B0, L = B1, L = B2
    for (int i = 0; i < 3; i++)
    {
        ra = a.mHalfWidth[0] * subExp.mat[0][i] + a.mHalfWidth[1] * subExp.mat[1][i]
            + a.mHalfWidth[2] * subExp.mat[2][i];
        rb = b.mHalfWidth[i];
        if (abs(trans[0] * rot.mat[0][i] + trans[1] * rot.mat[1][i] + trans[2] * rot.mat[2][i] > ra + rb))
        {
            return false;
        }
    }

    // Test axis L = A0 x B0
    ra = a.mHalfWidth[1] * subExp.mat[2][0] + a.mHalfWidth[2] * subExp.mat[1][0];
    rb = b.mHalfWidth[1] * subExp.mat[0][2] + b.mHalfWidth[2] * subExp.mat[0][1];
    if (abs(trans[2] * rot.mat[1][0] - trans[1] * rot.mat[2][0] > ra + rb))
    {
        return false;
    }

    // Test axis L = A0 x B1
    ra = a.mHalfWidth[1] * subExp.mat[2][1] + a.mHalfWidth[2] * subExp.mat[1][2];
    rb = b.mHalfWidth[0] * subExp.mat[0][2] + b.mHalfWidth[2] * subExp.mat[0][0];
    if (abs(trans[2] * rot.mat[1][1] - trans[1] * rot.mat[2][1]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A0 x B2
    ra = a.mHalfWidth[1] * subExp.mat[2][2] + a.mHalfWidth[2] * subExp.mat[1][2];
    rb = b.mHalfWidth[0] * subExp.mat[0][1] + b.mHalfWidth[1] * subExp.mat[0][0];
    if (abs(trans[2] * rot.mat[1][2] - trans[1] * rot.mat[2][2]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A1 x B0
    ra = a.mHalfWidth[0] * subExp.mat[2][0] + a.mHalfWidth[2] * subExp.mat[0][0];
    rb = b.mHalfWidth[1] * subExp.mat[1][2] + b.mHalfWidth[2] * subExp.mat[1][1];
    if (abs(trans[0] * rot.mat[2][0] - trans[2] * rot.mat[0][0]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A1 x B1
    ra = a.mHalfWidth[0] * subExp.mat[2][1] + a.mHalfWidth[2] * subExp.mat[0][1];
    rb = b.mHalfWidth[0] * subExp.mat[1][2] + b.mHalfWidth[2] * subExp.mat[1][0];
    if (abs(trans[0] * rot.mat[2][0] - trans[2] * rot.mat[0][0]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A1 x B2
    ra = a.mHalfWidth[0] * subExp.mat[2][2] + a.mHalfWidth[2] * subExp.mat[0][2];
    rb = b.mHalfWidth[0] * subExp.mat[1][1] + b.mHalfWidth[1] * subExp.mat[1][0];
    if (abs(trans[0] * rot.mat[2][2] - trans[2] * rot.mat[0][2]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A2 x B0
    ra = a.mHalfWidth[0] * subExp.mat[1][0] + a.mHalfWidth[1] * subExp.mat[0][0];
    rb = b.mHalfWidth[1] * subExp.mat[2][2] + b.mHalfWidth[2] * subExp.mat[2][1];
    if (abs(trans[1] * rot.mat[0][0] - trans[0] * rot.mat[1][0]))
    {
        return false;
    }

    // Test axis L = A2 x B1
    ra = a.mHalfWidth[0] * subExp.mat[1][1] + a.mHalfWidth[1] * subExp.mat[0][1];
    rb = b.mHalfWidth[0] * subExp.mat[2][2] + b.mHalfWidth[2] * subExp.mat[2][0];
    if (abs(trans[1] * rot.mat[0][1] - trans[0] * rot.mat[1][1]) > ra + rb)
    {
        return false;
    }

    // Test axis L = A2 x B2
    ra = a.mHalfWidth[0] * subExp.mat[1][2] + a.mHalfWidth[1] * subExp.mat[0][2];
    rb = b.mHalfWidth[0] * subExp.mat[2][1] + b.mHalfWidth[1] * subExp.mat[2][0]; 
    if (abs(trans[1] * rot.mat[0][2] - trans[0] * rot.mat[1][2]) > ra + rb)
    {
        return false;
    }

    return true;
}

// Ray-AABB Intersection test
bool Physics::Intersect(const Ray& ray, const AABB& box, Vector3* pHitPoint)
{	
	const float EPSILON = 0.001f;
	Vector3 dist = ray.mTo - ray.mFrom;
	Vector3 initPos = ray.mFrom;
	float tmin = 0.0f;
	float tmax = INFINITY;

	// Check the x component
	if (abs(dist.x) < EPSILON) 
	{
		if (initPos.x < box.mMin.x || initPos.x > box.mMax.x) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}
	else 
	{
		float tx1 = (box.mMin.x - initPos.x) / dist.x;
		float tx2 = (box.mMax.x - initPos.x) / dist.x;

		if (tx1 > tx2) 
		{
			float temp = tx1;
			tx1 = tx2;
			tx2 = temp;
		}

		tmin = Math::Max(tx1, tmin);
		tmax = Math::Min(tx2, tmax);
		if (tmin > tmax) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}

		if (tmin < 0 || tmax < 0 || tmin > 1 || tmax > 1) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}

	// Check the y component
	if (abs(dist.y) < EPSILON) 
	{
		if (initPos.y < box.mMin.y || initPos.y > box.mMax.y) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}
	else 
	{
		float ty1 = (box.mMin.y - initPos.y) / dist.y;
		float ty2 = (box.mMax.y - initPos.y) / dist.y;

		if (ty1 > ty2) 
		{
			float temp = ty1;
			ty1 = ty2;
			ty2 = temp;
		}

		tmin = Math::Max(ty1, tmin);
		tmax = Math::Min(ty2, tmax);
		if (tmin > tmax) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}

		if (tmin < 0 || tmax < 0 || tmin > 1 || tmax > 1) 
		{
			if (pHitPoint != nullptr) {
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}

	// Check the z component
	if (abs(dist.z) < EPSILON) 
	{
		if (initPos.z < box.mMin.z || initPos.z > box.mMax.z) 
		{
			if (pHitPoint != nullptr) {
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}
	else 
	{
		float tz1 = (box.mMin.z - initPos.z) / dist.z;
		float tz2 = (box.mMax.z - initPos.z) / dist.z;

		if (tz1 > tz2) 
		{
			float temp = tz1;
			tz1 = tz2;
			tz2 = temp;
		}

		tmin = Math::Max(tz1, tmin);
		tmax = Math::Min(tz2, tmax);
		if (tmin > tmax) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}

		if (tmin < 0 || tmax < 0 || tmin > 1 || tmax > 1) 
		{
			if (pHitPoint != nullptr) 
			{
				*pHitPoint = Vector3::Zero;
			}
			return false;
		}
	}

	if (pHitPoint != nullptr) 
	{
		*pHitPoint = initPos + dist * tmin;
	}
	return true;
}

// Sphere-Sphere Intersection test
bool Physics::Intersect(const BoundingSphere& a, const BoundingSphere& b)
{
    Vector3 vec = b.mCenter - a.mCenter;
    float dist = vec.LengthSq();
	return (dist <= a.mRadius + b.mRadius);
}

void Physics::AddObj(CollisionComponent* comp)
{
    mObj.push_back(comp);
}

void Physics::RemoveObj(CollisionComponent* pObj)
{
    const size_t len = mObj.size();
	for (int i = 0; i < len; i++) {
		if (mObj[i] == pObj) {
			mObj.erase(mObj.begin() + i);
			break;
		}
	}   
}

bool Physics::RayCast(const Ray& ray, Vector3* pHitPoint)
{
    float closestDist = INFINITY;
	*pHitPoint = Vector3::Zero;
	for (CollisionComponent* com : mObj)
    {
		AABB box = com->GetAABB();
		Vector3 temp = Vector3::Zero;

		if (Physics::Intersect(ray, box, &temp))
        {
			float tempDist = (ray.mFrom - temp).Length();

			if (tempDist < closestDist)
            {
				*pHitPoint = temp;
				closestDist = tempDist;
			}
		}
	}

	// That means the vector of CollisionComponent was empty or none of them intersected
	if (closestDist == INFINITY)
    {
		return false;
	}
	return true;
}
