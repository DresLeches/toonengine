#pragma once
#include "engineMath.h"
#include <vector>

class CollisionComponent;
struct Sphere;
struct AABB;
struct Ray;
struct OBB;
struct BoundingSphere;

class Physics {
public:
    static bool Intersect(const AABB& a, const AABB& b, AABB* pOverlap = nullptr);
    static bool Intersect(const OBB& a, const OBB& b, OBB* pOverlap = nullptr);
	static bool Intersect(const Ray& ray, const AABB& box, Vector3* pHitPoint = nullptr);
    static bool Intersect(const BoundingSphere& a, const BoundingSphere& b);

    bool RayCast(const Ray& ray, Vector3* pHitPoint = nullptr);
    void AddObj(CollisionComponent* pObj);
    void RemoveObj(CollisionComponent* pObj);

private:
    std::vector<CollisionComponent*> mObj;
};
