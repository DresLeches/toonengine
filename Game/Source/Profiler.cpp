#include "stdafx.h"
#include "Profiler.h"
#include <stdio.h>

static bool firstTimer = true;
static int numTimer = 0;
static FILE* profileJson;
static std::string json;

typedef Profiler::Timer Timer;
typedef Profiler::ScopedTimer ScopedTimer;

// Profiler methods
Profiler::Profiler() {    
    fopen_s(&profileJson, "profile.json", "w");
    fprintf(profileJson, "[");
}

Profiler::~Profiler() {
	FILE* profilePerf;
	fopen_s(&profilePerf, "profile.txt", "w");
	fprintf(profilePerf, "name:, avg (ms), max(ms)\n");

	int count = 0;
    for (auto& timer : mTimer) {
		fprintf(profilePerf, "%s:, %.6f, %.6f\n", timer.second->GetName().c_str(), 
												  timer.second->GetAvg_ms(), 
												  timer.second->GetMax_ms());

        delete timer.second;
    }
	
	fprintf(profileJson, json.substr(0, json.size() - 2).c_str());
    fprintf(profileJson, "]");
	fclose(profileJson);
	fclose(profilePerf);
}

Profiler* Profiler::Get() {
	static Profiler profile;
    return &profile;
}

Timer* Profiler::GetTimer(const std::string& name) {
    auto it = mTimer.find(name);
    if (it != mTimer.end()) {
        return it->second;
    }
    Timer* t = new Timer(name);
    mTimer[name] = t;
	numTimer++;
	return t;
}

void Profiler::ResetAll() {
    for (auto t : mTimer) {
        t.second->Reset();
    }
}

// Timer methods
Timer::Timer(std::string name)
:mName(name)
,mCurrent_ms(0.0)
,mMax_ms(0.0)
,mTotalTimes_ms(0.0)
,mNumSample(0)
{
}
Timer::~Timer()
{
}

void Timer::Start() {
    mStart = std::chrono::high_resolution_clock::now();
	
	
	if (!firstTimer) {
		fprintf(profileJson, ",\n{\"name\": \"%s\",\n", mName.c_str());
	}
	else {
		fprintf(profileJson, "{\"name\": \"%s\",\n", mName.c_str());
		firstTimer = false;
	}
    fprintf(profileJson, "\"ph\": \"B\",\n");
    fprintf(profileJson, "\"ts\": %.6f,\n", mStart.time_since_epoch().count() / 1000.0);
    fprintf(profileJson, "\"pid\": 1,\n");
	fprintf(profileJson, "\"tid\": 1\n}");
}

void Timer::Stop() {
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    double calcTime = (double) std::chrono::duration_cast<std::chrono::nanoseconds>(end - mStart).count();
    calcTime /= 1000000.0;
    mCurrent_ms += calcTime;

	
	// Let the profiler record the time
	fprintf(profileJson, ",\n{\"ph\": \"E\",\n");
	fprintf(profileJson, "\"ts\": %.6f,\n", end.time_since_epoch().count() / 1000.0);
	fprintf(profileJson, "\"pid\": 1,\n");
	fprintf(profileJson, "\"tid\": 1\n}");
}

void Timer::Reset() {
    mTotalTimes_ms += mCurrent_ms;
	mNumSample++;
	mMax_ms = (mCurrent_ms > mMax_ms) ? mCurrent_ms : mMax_ms;
    mCurrent_ms = 0;
}

const std::string& Timer::GetName() const {
    return mName;
}

double Timer::GetTime_ms() const {
    return mCurrent_ms;
}

double Timer::GetMax_ms() const {
    return mMax_ms;
}

double Timer::GetAvg_ms() const {
    return mTotalTimes_ms / mNumSample;
}

// Scoped Timer functions
ScopedTimer::ScopedTimer(Timer* timer)
:mTimer(timer)
{
    mTimer->Start();
}

ScopedTimer::~ScopedTimer() {
    mTimer->Stop();
}
