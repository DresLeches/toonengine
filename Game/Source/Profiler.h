#pragma once
#include "stdafx.h"
#define PROFILE_SCOPE(name) \
Profiler::ScopedTimer name##_scope(Profiler::Get()->GetTimer(std::string(#name)))

class Profiler {
public:

    // Timer
    class Timer {
    public:
        void Start();
        void Stop();
        void Reset();
        const std::string& GetName() const;
        double GetTime_ms() const;
        double GetMax_ms() const;
        double GetAvg_ms() const;
        
    private:
        Timer(std::string name);
        ~Timer();

        std::string mName;
        double mCurrent_ms;
        double mMax_ms;
        double mTotalTimes_ms;
        int mNumSample;
        std::chrono::high_resolution_clock::time_point mStart;
		
		friend class Profiler;
    };

    // Scope Timer
    class ScopedTimer {
    public:
        ScopedTimer(Timer* timer);
        ~ScopedTimer();

    private:
        Timer* mTimer;
    };
        
    // Member functions
	static Profiler* Get();
    Timer* GetTimer(const std::string& name);
    void ResetAll();

private:
    Profiler();
    ~Profiler();

    std::unordered_map<std::string, Timer*> mTimer;
	
};


