#include "stdafx.h"
#include "RenderObj.h"
#include "texture.h"
#include "mesh.h"
#include "engineMath.h"
#include "Component.h"

#define ARR_SIZE(arr) sizeof(arr) / sizeof(*arr)

RenderObj::RenderObj(Game* pGame, const Mesh* pMesh) 
:mGame(pGame)
,mMesh(pMesh)
{
	Graphics* graphics = mGame->GetGraphics();

	mObjectBuffer = graphics->CreateGraphicsBuffer(&mObjectData, sizeof(mObjectData),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_CPU_ACCESS_WRITE,
		D3D11_USAGE_DYNAMIC);
}

RenderObj::~RenderObj() {
    mObjectBuffer->Release();
    for (Component* comp : mComponents) 
	{
        delete comp;
    }
}

void RenderObj::AddComponent(Component* pComp) 
{
    mComponents.push_back(pComp);
}

void RenderObj::Update(float deltaTime) 
{
    for (Component* comp : mComponents) 
	{
        comp->Update(deltaTime);
    }
}

void RenderObj::Draw() 
{
    Graphics* graphics = mGame->GetGraphics();
    ID3D11DeviceContext* devCon = graphics->GetDeviceContext();

    graphics->UploadBuffer(mObjectBuffer, &mObjectData, sizeof(mObjectData));
    devCon->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_RENDEROBJ, 1, &mObjectBuffer);
	mMesh->Draw();
}
