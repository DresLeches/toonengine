#pragma once
#include "stdafx.h"
#include "game.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "Graphics.h"
#include "engineMath.h"

class Game;
class Texture;
class Mesh;
class Component;

class RenderObj {
public:
	RenderObj(Game* pGame, const Mesh* pMesh);
    virtual ~RenderObj();
    virtual void Draw();
    virtual void Update(float deltaTime);
    void AddComponent(Component* pComp);
    Game* GetGame() const { return mGame; }

    // Per object constants 
    struct PerObjectConstants {
        Matrix4 c_modelToWorld; 
    };

    PerObjectConstants mObjectData;

protected:
    Game* mGame;
    ID3D11Buffer* mObjectBuffer;
	const Mesh* mMesh;
    std::vector<Component*> mComponents;
};
