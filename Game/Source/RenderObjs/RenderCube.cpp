#include "stdafx.h"
#include "RenderCube.h"
#include "Game.h"
#include "mesh.h"
#include "Graphics.h"

#define ARR_SIZE(arr) sizeof(arr) / sizeof(*arr)
RenderCube::RenderCube(Game* pGame, const Shader* pShader, const Texture* pTex)
:RenderObj(pGame, nullptr)
{
    mCubeMesh = new Mesh(pGame, nullptr, pShader);
    mCubeMesh->SetTexture(Graphics::TEXTURE_SLOT_DIFFUSE, pTex);
}

RenderCube::~RenderCube() {
    delete mCubeMesh;
}


