#pragma once
#include "RenderObj.h"

class Mesh;

class RenderCube : public RenderObj {
public:
	RenderCube(Game* pGame, const Shader* pShader, const Texture* pTex);
	~RenderCube();

private:
    Mesh* mCubeMesh;
};
