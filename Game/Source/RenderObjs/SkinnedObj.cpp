#include "stdafx.h"
#include "SkinnedObj.h"

SkinnedObj::SkinnedObj(Game* pGame, const Mesh* pMesh)
:RenderObj(pGame, pMesh)
{
    Graphics* graphics = mGame->GetGraphics();
   
    mSkinBuffer = graphics->CreateGraphicsBuffer(&mSkinData, sizeof(mSkinData),
                                                 D3D11_BIND_CONSTANT_BUFFER,
                                                 D3D11_CPU_ACCESS_WRITE,
                                                 D3D11_USAGE_DYNAMIC);
}

SkinnedObj::~SkinnedObj() {
    mSkinBuffer->Release();
}

void SkinnedObj::Draw() {
    Graphics* graphics = mGame->GetGraphics();
    ID3D11DeviceContext* devCon = graphics->GetDeviceContext();

    graphics->UploadBuffer(mSkinBuffer, &mSkinData, sizeof(mSkinData));
    devCon->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_SKINNING, 1, &mSkinBuffer);
    RenderObj::Draw();
}
