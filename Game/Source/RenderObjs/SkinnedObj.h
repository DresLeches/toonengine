#pragma once
#include "engineMath.h"
#include "RenderObj.h"

#define MAX_SKELETON_BONES 80

class Game;

class SkinnedObj : public RenderObj {
public:
    SkinnedObj(Game* pGame, const Mesh* pMesh);
    ~SkinnedObj();
    void Draw();

    struct SkinConstants {
        Matrix4 c_skinMatrix[MAX_SKELETON_BONES];
    };
    
    SkinConstants mSkinData;
private:
    ID3D11Buffer* mSkinBuffer;
};

