#include "stdafx.h"
#include "Skeleton.h"
#include "game.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include "jsonUtil.h"
#include <fstream>
#include <sstream>

Skeleton::Skeleton()
{
}

Skeleton::~Skeleton() 
{    
}

void Skeleton::ComputeGlobalInvBindPose() 
{

    for (Bone b : mBones) 
	{
        Matrix4 boneToModel = b.mLocalBindPose.ToMatrix();
        if (b.mParent != -1) 
		{
            boneToModel *= mGlobalInvBindPoses[b.mParent];
        }
        mGlobalInvBindPoses.push_back(boneToModel);
    }

    for (Matrix4& mat: mGlobalInvBindPoses) {
        mat.Invert();
    }
}

Skeleton* Skeleton::StaticLoad(const WCHAR* fileName, Game* game) 
{
    Skeleton* sk = new Skeleton();
    
    if (false == sk->Load(fileName)) 
	{
        delete sk;
        return nullptr;
    }
    
    return sk;
}

bool Skeleton::Load(const WCHAR* fileName) 
{
    std::ifstream file(fileName);
    if (!file.is_open()) 
	{
        return false;
    }

    std::stringstream ss;
    ss << file.rdbuf();
    std::string content = ss.str();
    rapidjson::StringStream jsonStr(content.c_str());
    rapidjson::Document doc;
    doc.ParseStream(jsonStr);

    if (!doc.IsObject()) 
	{
        return false;
    }

    std::string str = doc["metadata"]["type"].GetString();
    int ver = doc["metadata"]["version"].GetInt();

    // Check metadata
    if (!doc["metadata"].IsObject() ||
        str != "itpskel" ||
        ver != 1)
    {
        return false;
    }

    // Get number of bones in mannequin
    int numBones = 0;
    const rapidjson::Value& boneCount = doc["bonecount"];
    if (!boneCount.IsNumber()) 
	{
        return false;
    }
    numBones = boneCount.GetInt();
    
    // Loop through the bone data
    const rapidjson::Value& bones = doc["bones"];
    if (!bones.IsArray() ||
        bones.Size() < 1 ||
        bones.Size() != numBones)
    {
        return false;
    }

    // Iterate through the individual bones
    for (rapidjson::SizeType i = 0; i < bones.Size(); i++) 
	{
        const rapidjson::Value& name = bones[i]["name"];
        const rapidjson::Value& parent = bones[i]["parent"];
        const rapidjson::Value& bindpose = bones[i]["bindpose"];

        if (!name.IsString() ||
            !parent.IsNumber() ||
            !bindpose.IsObject())
        {
            return false;
        }

        std::string boneName = "";
        int parentInd = -1;
        Quaternion boneQuat;
        Vector3 trans;

        GetStringFromJSON(bones[i], "name", boneName);
        parentInd = parent.GetInt();
        GetQuaternionFromJSON(bindpose, "rot", boneQuat);
        GetVectorFromJSON(bindpose, "trans", trans);

        Skeleton::Bone temp;
        temp.mName = boneName;
        temp.mParent = parentInd;
        temp.mLocalBindPose.mRotation = boneQuat;
        temp.mLocalBindPose.mTranslation = trans;
        mBones.push_back(temp);
    }

    // Compute the global inverse bind poses
    ComputeGlobalInvBindPose();
    return true;
}



