#pragma once
#include "BoneTransform.h"
#include "engineMath.h"
#include <vector>
#include <string>

class Game;

class Skeleton {
public:

	struct Bone {
		BoneTransform mLocalBindPose;
		std::string mName;
		int mParent;
	};

    Skeleton();
    ~Skeleton();

    static Skeleton* StaticLoad(const WCHAR* fileName, Game* game);
    bool Load(const WCHAR* fileName);

    size_t GetNumBones() const { return mBones.size(); }
    const Bone& GetBone(size_t idx) const { return mBones[idx]; }
    const std::vector<Bone>& GetBones() const { return mBones; }
    const std::vector<Matrix4>& GetGlobalInvBindPoses() const { return mGlobalInvBindPoses; }

private:
	void ComputeGlobalInvBindPose();

	std::vector<Bone> mBones;
	std::vector<Matrix4> mGlobalInvBindPoses;
    
};
