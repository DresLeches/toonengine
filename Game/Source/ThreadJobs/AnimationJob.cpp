#include "stdafx.h"
#include "AnimationJob.h"
#include "Components/Character.h"
#include "engineMath.h"
#include "Animation.h"
#include "Skeleton.h"
#include "RenderObjs/SkinnedObj.h"
#include <vector>

AnimationJob::AnimationJob(Character* c)
:mCharacter(c)
{
}

AnimationJob::~AnimationJob() 
{    
}

void AnimationJob::DoIt() 
{
    std::vector<Matrix4> globalPoses;
    std::vector<Matrix4> globalInvPoses;
	mCharacter->ChangeState();
    mCharacter->mCurrentAnim->GetGlobalPoseAtTime(globalPoses,
                                                  mCharacter->mSkeleton,
                                                  mCharacter->mAnimationTime);
    globalInvPoses = mCharacter->mSkeleton->GetGlobalInvBindPoses();

    const size_t numPoses = globalPoses.size();
    for (size_t i = 0; i < numPoses; i++) 
	{
        mCharacter->mSkinnedObj->mSkinData.c_skinMatrix[i] = globalInvPoses[i] * globalPoses[i];
    }   
}


