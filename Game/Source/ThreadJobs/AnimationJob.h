#pragma once
#include "Job.h"

class Character;

class AnimationJob : public Job {
public:
    AnimationJob(Character* c);
    ~AnimationJob();

    void DoIt();
private:
    Character* mCharacter;
};
