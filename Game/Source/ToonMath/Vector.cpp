#include "stdafx.h"
#include "Vector.h"
#include <cmath>

/****************************************************/
/*    struct Vector2 functions                      */
/****************************************************/
const Vector2 Vector2::Zero(0.0f, 0.0f);
const Vector2 Vector2::One(1.0f, 1.0f);
const Vector2 Vector2::UnitX(1.0f, 0.0f);
const Vector2 Vector2::UnitY(0.0f, 1.0f);

Vector2::Vector2()
    : x(0.0f)
    , y(0.0f)
{
}

Vector2::Vector2(float inX, float inY)
    : x(inX)
    , y(inY)
{
}

inline void Vector2::Set(float inX, float inY)
{
    x = inX;
    y = inY;
}

inline float Vector2::LengthSq() const
{
    return (x*x + y*y);   
}

inline float Vector2::Length() const
{
    return (sqrtf(LengthSq()));   
}

inline void Vector2::Normalize()
{
    float length = Length();
    x /= length;
    y /= length;   
}

inline Vector2 Vector2::Normalize(const Vector2& vec)
{
    Vector2 temp = vec;
    temp.Normalize();
    return temp;   
}

inline float Vector2::Dot(const Vector2& a, const Vector2& b)
{
    return a.x * b.x + a.y * b.y;   
}

inline Vector2 Vector2::Lerp(const Vector2& a, const Vector2& b, float f)
{
    return a + f * (b - a);   
}

/****************************************************/
/*    struct Vector3 functions                      */
/****************************************************/
const Vector3 Vector3::Zero(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::One(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::UnitX(1.0f, 0.0f, 0.0f);
const Vector3 Vector3::UnitY(0.0f, 1.0f, 0.0f);
const Vector3 Vector3::UnitZ(0.0f, 0.0f, 1.0f);

Vector3::Vector3()
    : x(0.0f)
    , y(0.0f)
    , z(0.0f)
{
}

Vector3::Vector3(float inX, float inY, float inZ)
    : x(inX)
    , y(inY)
    , z(inZ)
{
}

inline void Vector3::Set(float inX, float inY, float inZ)
{
    x = inX;
    y = inY;
    z = inZ;
}

inline float Vector3::LengthSq() const
{
    return (x*x + y*y + z*z);
}

inline float Vector3::Length() const
{
    return (sqrtf(LengthSq()));
}

inline void Vector3::Normalize()
{
    float length = Length();
    x /= length;
    y /= length;
    z /= length;
}

inline Vector3 Vector3::Normalize(const Vector3& vec)
{
    Vector3 temp = vec;
    temp.Normalize();
    return temp;
}

inline float Vector3::Dot(const Vector3& a, const Vector3& b)
{
    return (a.x * b.x + a.y * b.y + a.z * b.z);
}

inline Vector3 Vector3::Lerp(const Vector3& a, const Vector3& b, float f)
{
    return Vector3(a + f * (b - a));
}
/****************************************************/
/*    struct Vector4 functions                      */
/****************************************************/
const Vector4 Vector4::Zero(0.0f, 0.0f, 0.0f, 0.0f);
const Vector4 Vector4::One(1.0f, 1.0f, 1.0f, 1.0f);
const Vector4 Vector4::UnitX(1.0f, 0.0f, 0.0f, 0.0f);
const Vector4 Vector4::UnitY(0.0f, 1.0f, 0.0f, 0.0f);
const Vector4 Vector4::UnitZ(0.0f, 0.0f, 1.0f, 0.0f);
const Vector4 Vector4::UnitW(0.0f, 0.0f, 0.0f, 1.0f);

Vector4::Vector4()
    : x(0.0f)
    , y(0.0f)
    , z(0.0f)
    , w(0.0f)
{
}

Vector4::Vector4(float inX, float inY, float inZ, float inW)
    : x(inX)
    , y(inY)
    , z(inZ)
    , w(inW)
{
}

// Set all four components in one line
inline void Vector4::Set(float inX, float inY, float inZ, float inW)
{
    x = inX;
    y = inY;
    z = inZ;
    w = inW;
}

// Length squared of vector
inline float Vector4::LengthSq() const
{
    return (x*x + y*y + z*z + w*w);
}

// Length of vector
inline float Vector4::Length() const
{
    return (sqrtf(LengthSq()));
}

// Normalize this vector
inline void Vector4::Normalize()
{
    float length = Length();
    x /= length;
    y /= length;
    z /= length;
    w /= length;
}

// Normalize the provided vector
inline Vector4 Vector4::Normalize(const Vector4& vec)
{
    Vector4 temp = vec;
    temp.Normalize();
    return temp;
}

// Lerp from A to B by f
inline Vector4 Vector4::Lerp(const Vector4& a, const Vector4& b, float f)
{
    return Vector4(a + f * (b - a));
}


