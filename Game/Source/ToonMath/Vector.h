#pragma once

struct Quaternion;

struct Vector2
{
    float x;
    float y;

    Vector2();
    explicit Vector2(float inX, float inY);
    void Set(float inX, float inY);
    float LengthSq() const;
    float Length() const;
    void Normalize();
    static Vector2 Normalize(const Vector2& vec);
    static float Dot(const Vector2& a, const Vector2& b);
    static Vector2 Lerp(const Vector2& a, const Vector2& b, float f);

    friend Vector2 operator+(const Vector2& a, const Vector2& b)
    {
        return Vector2(a.x + b.x, a.y + b.y);
    }

    Vector2& operator+=(const Vector2& right)
    {
        x += right.x;
        y += right.y;
        return *this;
    }

    friend Vector2 operator-(const Vector2& a, const Vector2& b)
    {
        return Vector2(a.x - b.x, a.y - b.y);
    }

    Vector2& operator-=(const Vector2& right)
    {
        x -= right.x;
        y -= right.y;
        return *this;
    }

    friend Vector2 operator*(const Vector2& a, const Vector2& b)
    {
        return Vector2(a.x * b.x, a.y * b.y);
    }

    friend Vector2 operator*(const Vector2& vec, float scalar)
    {
        return Vector2(vec.x * scalar, vec.y * scalar);
    }

    friend Vector2 operator*(float scalar, const Vector2& vec)
    {
        return Vector2(vec.x * scalar, vec.y * scalar);
    }

    Vector2& operator*=(float scalar)
    {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    friend Vector2 operator/(const Vector2& vec, float scalar)
    {
        return Vector2(vec.x / scalar, vec.y / scalar);
    }

    Vector2& operator/=(float scalar)
    {
        x /= scalar;
        y /= scalar;
        return *this;
    }
    static const Vector2 Zero;
    static const Vector2 One;
    static const Vector2 UnitX;
    static const Vector2 UnitY;   
};

struct Vector3
{
    float x;
    float y;
    float z;

    Vector3();
    explicit Vector3(float inX, float inY, float inZ);
    void Set(float inX, float inY, float inZ);
    float LengthSq() const;
    float Length() const;
    void Normalize();
    static Vector3 Normalize(const Vector3& vec);
    static float Dot(const Vector3& a, const Vector3& b);
    static Vector3 Lerp(const Vector3& a, const Vector3& b, float f);
	friend Vector3 Cross(const Vector3& a, const Vector3& b)
	{
		Vector3 vec;
		vec.x = (a.y * b.z - b.y * a.z);
		vec.y = (a.z * b.x - b.z * a.x);
		vec.z = (a.x * b.y - b.x * a.y);
		return vec;
	}
    
    friend Vector3 operator+(const Vector3& a, const Vector3& b)
    {
        return Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    friend Vector3 operator-(const Vector3& a, const Vector3& b)
    {
        return Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    friend Vector3 operator*(const Vector3& left, const Vector3& right)
    {
        return Vector3(left.x * right.x, left.y * right.y, left.z * right.z);
    }

    friend Vector3 operator*(const Vector3& vec, float scalar)
    {
        return Vector3(vec.x * scalar, vec.y * scalar, vec.z * scalar);
    }

    friend Vector3 operator*(float scalar, const Vector3& vec)
    {
        return Vector3(vec.x * scalar, vec.y * scalar, vec.z * scalar);
    }

    Vector3& operator*=(float scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    friend Vector3 operator/(const Vector3& vec, float scalar)
    {
        return Vector3(vec.x / scalar, vec.y / scalar, vec.z / scalar);
    }

    Vector3& operator/=(float scalar)
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        return *this;
    }

    Vector3& operator+=(const Vector3& right)
    {
        x += right.x;
        y += right.y;
        z += right.z;
        return *this;
    }

    Vector3& operator-=(const Vector3& right)
    {
        x -= right.x;
        y -= right.y;
        z -= right.z;
        return *this;
    }

    static const Vector3 Zero;
    static const Vector3 One;
    static const Vector3 UnitX;
    static const Vector3 UnitY;   
    static const Vector3 UnitZ;
};


struct Vector4
{
    float x;
    float y;
    float z;
    float w;

    Vector4();
    explicit Vector4(float inX, float inY, float inZ, float inW);
    void Set(float inX, float inY, float inZ, float inW);
    float LengthSq() const;
    float Length() const;
    void Normalize();
    static Vector4 Normalize(const Vector4& vec);
    static float Dot(const Vector4& a, const Vector4& b);
    static Vector4 Lerp(const Vector4& a, const Vector4& b, float f);

    friend Vector4 operator+(const Vector4& a, const Vector4& b)
    {
        return Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
    }

    friend Vector4 operator-(const Vector4& a, const Vector4& b)
    {
        return Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
    }

    friend Vector4 operator*(const Vector4& left, const Vector4& right)
    {
        return Vector4(left.x * right.x, left.y * right.y, left.z * right.z, left.w * right.w);
    }

    friend Vector4 operator*(const Vector4& vec, float scalar)
    {
        return Vector4(vec.x * scalar, vec.y * scalar, vec.z * scalar, vec.w * scalar);
    }

    friend Vector4 operator*(float scalar, const Vector4& vec)
    {
        return Vector4(vec.x * scalar, vec.y * scalar, vec.z * scalar, vec.w * scalar);
    }

    Vector4& operator*=(float scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        w *= scalar;
        return *this;
    }

    friend Vector4 operator/(const Vector4& vec, float scalar)
    {
        return Vector4(vec.x / scalar, vec.y / scalar, vec.z / scalar, vec.w / scalar);
    }

    Vector4& operator/=(float scalar)
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        w /= scalar;
        return *this;
    }

    Vector4& operator+=(const Vector4& right)
    {
        x += right.x;
        y += right.y;
        z += right.z;
        w += right.w;
        return *this;
    }

    Vector4& operator-=(const Vector4& right)
    {
        x -= right.x;
        y -= right.y;
        z -= right.z;
        w -= right.w;
        return *this;
    }
    static const Vector4 Zero;
    static const Vector4 One;
    static const Vector4 UnitX;
    static const Vector4 UnitY;   
    static const Vector4 UnitZ;
    static const Vector4 UnitW;
};
