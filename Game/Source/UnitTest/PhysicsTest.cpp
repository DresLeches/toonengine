#include "stdafx.h"
#include "engineMath.h"
#include "UnitTest/PhysicsTest.h"
#include "Collider/BoundingVolumes.h"
#include "Collider/Segments.h"
#include "Physics.h"

bool PhysicsUnitTest::Test_AABB_AABB_Intersection()
{

    // Test the AABB intersection function
    const TestAABB testAABB[] =
    {
        {
            AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f)),
            AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-110.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f)),
            AABB(Vector3(-100.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(110.0f, 10.0f, 10.0f)),
            AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(100.0f, 10.0f, 10.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -110.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f)),
            AABB(Vector3(-10.0f, -100.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 110.0f, 10.0f)),
            AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 100.0f, 10.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -10.0f, -110.0f), Vector3(10.0f, 10.0f, -90.0f)),
            AABB(Vector3(-10.0f, -10.0f, -100.0f), Vector3(10.0f, 10.0f, -90.0f))
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 110.0f)),
            AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 100.0f))
        }, 
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-120.0f, -10.0f, -10.0f), Vector3(-110.0f, 10.0f, 10.0f)),
            AABB(Vector3::One, Vector3::Zero)
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(110.0f, -10.0f, -10.0f), Vector3(120.0f, 10.0f, 10.0f)), 
            AABB(Vector3::One, Vector3::Zero)
        }, 
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -120.0f, -10.0f), Vector3(10.0f, -110.0f, 10.0f)),
            AABB(Vector3::One, Vector3::Zero)
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, 110.0f, -10.0f), Vector3(10.0f, 120.0f, 10.0f)),
            AABB(Vector3::One, Vector3::Zero)
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -10.0f, -120.0f), Vector3(10.0f, -10.0f, -110.0f)),
            AABB(Vector3::One, Vector3::Zero)
        },
        {
            AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
            AABB(Vector3(-10.0f, -10.0f, 110.0f), Vector3(10.0f, 10.0f, 120.0f)),
            AABB(Vector3::One, Vector3::Zero)
        },
    };

    // Check the AABB-AABB Intersection function
	const size_t sizeAABB = sizeof(testAABB) / sizeof(testAABB[0]);
	bool isOk = true;
	for (int i = 0; i < sizeAABB; i++) {
		AABB res;
		AABB expected = testAABB[i].overlap;

		Physics::Intersect(testAABB[i].a, testAABB[i].b, &res);
		isOk &= Math::IsCloseEnuf(res.mMin.x, expected.mMin.x);
		isOk &= Math::IsCloseEnuf(res.mMin.y, expected.mMin.y);
		isOk &= Math::IsCloseEnuf(res.mMin.z, expected.mMin.z);
		isOk &= Math::IsCloseEnuf(res.mMax.x, expected.mMax.x);
		isOk &= Math::IsCloseEnuf(res.mMax.y, expected.mMax.y);
		isOk &= Math::IsCloseEnuf(res.mMax.z, expected.mMax.z);
	}
	return isOk;
}

bool PhysicsUnitTest::Test_RayCast_ABB_Intersection()
{

	// Test ray AABB intersection function
	const TestRay testRay[] =
	{
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(-110.0f, 0.0f, 0.0f), Vector3(-90.0f, 0.0f, 0.0f)),
			true,
			Vector3(-100.0f, 0.0f, 0.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, -110.0f, 0.0f), Vector3(0.0f, -90.0f, 0.0f)),
			true,
			Vector3(0.0f, -100.0f, 0.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 0.0f, -110.0f), Vector3(0.0f, 0.0f, -90.0f)),
			true,
			Vector3(0.0f, 0.0f, -100.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(110.0f, 0.0f, 0.0f), Vector3(90.0f, 0.0f, 0.0f)),
			true,
			Vector3(100.0f, 0.0f, 0.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 110.0f, 0.0f), Vector3(0.0f, 90.0f, 0.0f)),
			true,
			Vector3(0.0f, 100.0f, 0.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 0.0f, 110.0f), Vector3(0.0f, 0.0f, 90.0f)),
			true,
			Vector3(0.0f, 0.0f, 100.0f)
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(-120.0f, 0.0f, 0.0f), Vector3(-110.0f, 0.0f, 0.0f)),
			false,
			Vector3::Zero
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, -120.0f, 0.0f), Vector3(0.0f, -110.0f, 0.0f)),
			false,
			Vector3::Zero
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 0.0f, -120.0f), Vector3(0.0f, 0.0f, -110.0f)),
			false,
			Vector3::Zero
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(120.0f, 0.0f, 0.0f), Vector3(110.0f, 0.0f, 0.0f)),
			false,
			Vector3::Zero
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 120.0f, 0.0f), Vector3(0.0f, 110.0f, 0.0f)),
			false,
			Vector3::Zero
		},
		{
			AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
			Ray(Vector3(0.0f, 0.0f, 120.0f), Vector3(0.0f, 0.0f, 110.0f)),
			false,
			Vector3::Zero
		},
	};

	const size_t sizeRay = sizeof(testRay) / sizeof(testRay[0]);
	bool isOk = true;
	for (int i = 0; i < sizeRay; i++) {
		Vector3 res;

		isOk &= Physics::Intersect(testRay[i].ray, testRay[i].box, &res);
		isOk = !(isOk ^ testRay[i].hit);
		isOk &= Math::IsCloseEnuf(testRay[i].point.x, res.x);
		isOk &= Math::IsCloseEnuf(testRay[i].point.y, res.y);
		isOk &= Math::IsCloseEnuf(testRay[i].point.z, res.z);
	}
	return isOk;
}

// TODO: How to write unit testing for OBB?
bool PhysicsUnitTest::Test_OBB_OBB_Intersection()
{
	return false;
}



