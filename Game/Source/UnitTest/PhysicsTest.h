#pragma once
#include "Collider/BoundingVolumes.h"
#include "Collider/Segments.h"

struct TestAABB { 
    AABB a;         
    AABB b;         
    AABB overlap; 
};

struct TestOBB {
    OBB a;
    OBB b;
};

struct TestRay {
	AABB box;
	Ray ray;
	bool hit;
	Vector3 point;
};

namespace PhysicsUnitTest {
    bool Test_AABB_AABB_Intersection();
	bool Test_RayCast_ABB_Intersection();
    bool Test_OBB_OBB_Intersection();
};