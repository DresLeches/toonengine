#include "stdafx.h"
#include "VertexBuffer.h"

// Macros for debugging
#define BUFFER_DEBUG 0

VertexBuffer::VertexBuffer(Graphics* graphics, const void* vertData, uint32_t vertCount, uint32_t vertStride, 
                                               const void* indexData, uint32_t indexCount, uint32_t indexStride) 
:mVertStride(vertStride)
,mIndexCount(indexCount)
,mGraphics(graphics)
{
    if (indexStride == sizeof(uint16_t)) {
        mIndexFormat = DXGI_FORMAT_R16_UINT;
    }
    else {
        mIndexFormat = DXGI_FORMAT_R32_UINT;
    }
    mVertBuff = mGraphics->CreateGraphicsBuffer(vertData, vertCount * vertStride, 
                                                D3D11_BIND_VERTEX_BUFFER, 
												D3D11_CPU_ACCESS_WRITE, 
												D3D11_USAGE_DYNAMIC);
    mIndexBuffer = mGraphics->CreateGraphicsBuffer(indexData, indexCount * indexStride, 
                                                   D3D11_BIND_INDEX_BUFFER, 
												   D3D11_CPU_ACCESS_WRITE, 
												   D3D11_USAGE_DYNAMIC);
}

VertexBuffer::~VertexBuffer() {
    mVertBuff->Release();
    mIndexBuffer->Release();
}

void VertexBuffer::SetActive() const {
    ID3D11DeviceContext* devCon = mGraphics->GetDeviceContext();
    const UINT vertOffsets = 0;
    const UINT indexOffsets = 0;
    devCon->IASetIndexBuffer(mIndexBuffer, mIndexFormat, indexOffsets);
    devCon->IASetVertexBuffers(0, 1, &mVertBuff, &mVertStride, &vertOffsets);
}

void VertexBuffer::Draw() const {
    ID3D11DeviceContext* devCon = mGraphics->GetDeviceContext();
    devCon->DrawIndexed(mIndexCount, 0, 0);
}
