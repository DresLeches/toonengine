#pragma once
#include "stdafx.h"
#include "Graphics.h"

class VertexBuffer {
public:
    VertexBuffer(Graphics* graphics, const void* vertData, uint32_t vertCount, uint32_t vertStride, 
                                     const void* indexData, uint32_t indexCount, uint32_t indexStride);
    ~VertexBuffer();
    void SetActive() const;
    void Draw() const; 

private:
    ID3D11Buffer* mVertBuff;
    ID3D11Buffer* mIndexBuffer;
    DXGI_FORMAT mIndexFormat;
    uint32_t mIndexCount;
    uint32_t mVertStride;       // Bytes per vertex
    Graphics* mGraphics; 
};