#include "stdafx.h"
#include "Worker.h"
#include "Job.h"
#include "JobManager.h"

static bool keepRunning = true;

void Worker::Begin() 
{
    mWorkerThread = std::thread(Loop);
}

void Worker::End() 
{
    mWorkerThread.join();
}

void Worker::Loop() 
{
	static JobManager* jb = JobManager::GetJobManager();

	// Put thread in a spinlock until shutdown signal
	while (keepRunning) 
	{
        Job* j = jb->GetAvailableJob();
        if (j != nullptr) 
		{
            j->DoIt();
            jb->WorkerFinishedJob();
			delete j;	// Delete since we're done with it
        }
    }
}

void Worker::SignalThreadAll() 
{
	keepRunning = false;
}
