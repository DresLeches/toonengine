#pragma once
#include <thread>

class JobManager;

class Worker {
public:
    void Begin();
    void End();
    static void Loop();

private:
    std::thread mWorkerThread;

   // static JobManager* mJobManager;
	static void SignalThreadAll();
	friend class JobManager;
};
