#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "RenderObjs/RenderCube.h"
#include "RenderObjs/SkinnedObj.h"
#include "texture.h"
#include "mesh.h"
#include "stringUtil.h"
#include "jsonUtil.h"
#include "Component.h"
#include "Components/PointLight.h"
#include "Components/Character.h"
#include "Components/followCam.h"
#include "Components/player.h"
#include "Components/SimpleRotate.h"
#include "Components/CollisionComponent.h"
#include "Profiler.h"
#include "JobManager.h"
#include "InputLayout.h"
#include "GameDebug.h"
#include "Physics.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>

#define ARR_SIZE(arr) sizeof(arr) / sizeof(*arr)


Game::Game()
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
	mGraphics = new Graphics();
	mGraphics->InitD3D(hWnd, width, height);
	
	ID3D11Device* dev = mGraphics->GetDevice();
	ID3D11DeviceContext* devCon = mGraphics->GetDeviceContext();
    
	mShaderCache = new AssetCache<Shader>(this);
	mShaderCache->Cache(L"Mesh", new Shader(mGraphics));
	mShaderCache->Cache(L"Phong", new Shader(mGraphics));
	mShaderCache->Cache(L"Unlit", new Shader(mGraphics));
    mShaderCache->Cache(L"Skinned", new Shader(mGraphics));
    mShaderCache->Cache(L"Cel", new Shader(mGraphics));
	mShaderCache->Cache(L"Normal", new Shader(mGraphics));

	GetShader(L"Mesh")->Load(L"Shaders/Mesh.hlsl", InputLayout::inputElem, 2);
    GetShader(L"Phong")->Load(L"Shaders/Phong.hlsl", InputLayout::inputMesh, 3);
	GetShader(L"Unlit")->Load(L"Shaders/Unlit.hlsl", InputLayout::inputMesh, 3);
    GetShader(L"Skinned")->Load(L"Shaders/Skinned.hlsl", InputLayout::inputSkinnedMesh, 5);
    GetShader(L"Cel")->Load(L"Shaders/Cel.hlsl", InputLayout::inputMesh, 3);
	GetShader(L"Normal")->Load(L"Shaders/Normal.hlsl", InputLayout::inputNormal, 4);
    
    mCamera = new Camera(mGraphics);
    mTextureCache = new AssetCache<Texture>(this);	
	mMeshCache = new AssetCache<Mesh>(this);
    mSkeletonCache = new AssetCache<Skeleton>(this);
    mAnimationCache = new AssetCache<Animation>(this);

	ZeroMemory(&mLightingData, sizeof(mLightingData));
	mLightingBuffer = mGraphics->CreateGraphicsBuffer(&mLightingData, sizeof(mLightingData),
														D3D11_BIND_CONSTANT_BUFFER,
														D3D11_CPU_ACCESS_WRITE,
														D3D11_USAGE_DYNAMIC);
#if ROBOT_ARMY
	LoadLevel(L"Assets/Levels/Level08.itplevel");
#else
    LoadLevel(L"Assets/Levels/Level10.itplevel");
#endif

	JobManager::GetJobManager()->Begin();
}

void Game::Shutdown()
{
#if MEM_LEAK
	ID3D11Debug* memDebug = nullptr;
	mGraphics->GetDevice()->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&memDebug));
#endif
	JobManager::GetJobManager()->End();
	mGraphics->CleanD3D();
	mLightingBuffer->Release();

	for (RenderObj* obj : mRenderObj) 
	{
		delete obj;
	}

	delete mCamera;
	delete mGraphics;
	delete mShaderCache;
	delete mTextureCache;
	delete mMeshCache;
	delete mSkeletonCache;
	delete mAnimationCache;

#if MEM_LEAK
	memDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);	
	memDebug->Release();
#endif
}

void Game::Update(float deltaTime)
{
    for (RenderObj* obj : mRenderObj) 
	{
        obj->Update(deltaTime);
    }
	JobManager::GetJobManager()->WaitForJobs();
}

void Game::RenderFrame()
{	
	ID3D11DeviceContext* devCon = mGraphics->GetDeviceContext();
	Graphics::Color4 c(0.0f, 0.2f, 0.4f);

#if TOGGLE_PROFILER
	{
		PROFILE_SCOPE(BeginFrame);
		mGraphics->BeginFrame(c);
	}

	{
		PROFILE_SCOPE(UploadData);
		mGraphics->UploadBuffer(mLightingBuffer, &mLightingData, sizeof(mLightingData));
		devCon->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_LIGHTING, 1, &mLightingBuffer);
		mCamera->SetActive();
	}

	{
		PROFILE_SCOPE(Draw);
		for (RenderObj* obj : mRenderObj) 
		{
			obj->Draw();
		}
	}

	{
		PROFILE_SCOPE(EndFrame);
		mGraphics->EndFrame();
	}

#else
	mGraphics->BeginFrame(c);
	mGraphics->UploadBuffer(mLightingBuffer, &mLightingData, sizeof(mLightingData));
	devCon->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_LIGHTING, 1, &mLightingBuffer);
	mCamera->SetActive();
	for (RenderObj* obj : mRenderObj) 
	{
		obj->Draw();
	}
	mGraphics->EndFrame();

#endif
}

PointLightData* Game::AllocateLight() 
{
	for (int i = 0; i < MAX_POINT_LIGHTS; i++) 
	{
		if (!mLightingData.c_pointLight[i].mIsEnabled) 
		{
			mLightingData.c_pointLight[i].mIsEnabled = true;
			return &mLightingData.c_pointLight[i];
		}
	}
	return nullptr;
}

void Game::FreeLight(PointLightData* pLight) 
{
	pLight->mIsEnabled = false;
}

void Game::SetAmbientLight(const Vector3& color) 
{
	mLightingData.c_ambient = color;
}

const Vector3& Game::GetAmbientLight() const 
{
	return mLightingData.c_ambient;
}

Shader* Game::GetShader(const std::wstring& shaderName) 
{
	return mShaderCache->Get(shaderName);
}

Texture* Game::LoadTexture(const std::wstring& fileName) 
{
	return mTextureCache->Load(fileName);
}

Mesh* Game::LoadMesh(const std::wstring& fileName) 
{
	return mMeshCache->Load(fileName);
}

Skeleton* Game::LoadSkeleton(const std::wstring& fileName) 
{
    return mSkeletonCache->Load(fileName);
}

Animation* Game::LoadAnimation(const std::wstring& fileName) 
{
    return mAnimationCache->Load(fileName);
}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}
    
    // Load Camera data. Make sure to check the parsed json is a quaternion
    const rapidjson::Value& posJson = doc["camera"]["position"];
    const rapidjson::Value& rotJson = doc["camera"]["rotation"];
    if (!doc["camera"].IsObject() ||
        !posJson.IsArray() ||
        !rotJson.IsArray() ||
        rotJson.Size() != 4)
    {
        return false;
    }
    
    Quaternion cameraQ;
    Vector3 cameraPos;
    GetVectorFromJSON(doc["camera"], "position", cameraPos);
    GetQuaternionFromJSON(doc["camera"], "rotation", cameraQ);

    Matrix4 temp = Matrix4::CreateFromQuaternion(cameraQ) *
                   Matrix4::CreateTranslation(cameraPos);
    temp.Invert();
    mCamera->SetViewMatrix(temp);
    
    // Load the lighting data 
    const rapidjson::Value& lightJson = doc["lightingData"]["ambient"];
    if (!doc["lightingData"].IsObject() ||
        !lightJson.IsArray() ||
        lightJson.Size() != 3)
    {
        return false;
    }

    Vector3 ambient;
    GetVectorFromJSON(doc["lightingData"], "ambient", ambient);
    SetAmbientLight(ambient);

    // Render object
    const rapidjson::Value& rendObj = doc["renderObjects"];
    if (!rendObj.IsArray()) 
	{
        return false;
    }

    for (rapidjson::SizeType i = 0; i < rendObj.Size(); i++) 
	{

        const rapidjson::Value& objPosJson = rendObj[i]["position"];
        const rapidjson::Value& objRotJson = rendObj[i]["rotation"];
        const rapidjson::Value& objScaleJson = rendObj[i]["scale"];
        const rapidjson::Value& objMeshJson = rendObj[i]["mesh"];
        const rapidjson::Value& objCompJson = rendObj[i]["components"];

        if (!objPosJson.IsArray() ||
            objPosJson.Size() != 3 ||
            !objRotJson.IsArray() ||
            objRotJson.Size() != 4 ||
            !objScaleJson.IsNumber() ||
            !objMeshJson.IsString() ||
            !objCompJson.IsArray())
        {
            return false;
        }

        Quaternion objQuat;
        float objScale;
        std::wstring meshName;
        Vector3 objPos;
        GetVectorFromJSON(rendObj[i], "position", objPos);
        GetQuaternionFromJSON(rendObj[i], "rotation", objQuat);
        GetFloatFromJSON(rendObj[i], "scale", objScale);
        GetWStringFromJSON(rendObj[i], "mesh", meshName);

        Mesh* loadedMesh = LoadMesh(meshName);
        RenderObj* obj = nullptr;
		SkinnedObj* sObj = nullptr;
        if (loadedMesh->IsSkinned()) {
            sObj = new SkinnedObj(this, loadedMesh);
			mRenderObj.push_back(sObj);
			sObj->mObjectData.c_modelToWorld = Matrix4::CreateScale(objScale) *
				Matrix4::CreateFromQuaternion(objQuat) *
				Matrix4::CreateTranslation(objPos);
        }
        else {
            obj = new RenderObj(this, loadedMesh);
			mRenderObj.push_back(obj);
			obj->mObjectData.c_modelToWorld = Matrix4::CreateScale(objScale) *
				Matrix4::CreateFromQuaternion(objQuat) *
				Matrix4::CreateTranslation(objPos);
        }

        for (rapidjson::SizeType i = 0; i < objCompJson.Size(); i++) 
		{
            const rapidjson::Value& compType = objCompJson[i]["type"];
            
            std::string type = "";
            GetStringFromJSON(objCompJson[i], "type", type);
            if (type == "PointLight") 
			{
                Component* com = new PointLight(obj);
                com->LoadProperties(objCompJson[i]);
                obj->AddComponent(com);
            }
            else if (type == "Player") 
			{
                Component* com = new Player(sObj);
                com->LoadProperties(objCompJson[i]);
                sObj->AddComponent(com);
            }
            else if (type == "Character") 
			{
                Component* com = new Character(sObj);
                com->LoadProperties(objCompJson[i]);
                sObj->AddComponent(com);
            }
            else if (type == "FollowCam") 
			{
                Component* com = new FollowCam(sObj);
                com->LoadProperties(objCompJson[i]);
                sObj->AddComponent(com);
            }
            else if (type == "SimpleRotate") 
			{
                Component* com = new SimpleRotate(obj);
                com->LoadProperties(objCompJson[i]);
                obj->AddComponent(com);
            }
			else if (type == "Collision") 
			{
				CollisionComponent* com = new CollisionComponent(obj);
				com->LoadProperties(objCompJson[i]);
				mPhysics.AddObj(com);
			}
        }
    }
	return true;
}
