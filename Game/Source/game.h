#pragma once
#include "stdafx.h"
#include "Graphics.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "RenderObj.h"
#include "Camera.h"
#include "Skeleton.h"
#include "Animation.h"
//#include "assetCache.h"
#include "newAssetCache.h"
#include "LightingData.h"
#include "JobManager.h"
#include "Physics.h"
#include "Job.h"
#include <vector>

class RenderObj;
class Texture;
class Mesh;
class Physics;

// Game class
class Game
{
public:
    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	Graphics* GetGraphics() const { return mGraphics; }
	Shader* GetShader(const std::wstring& shaderName);
    Camera* GetCamera() const { return mCamera; }
	Texture* LoadTexture(const std::wstring& fileName);
	Mesh* LoadMesh(const std::wstring& fileName);
    Skeleton* LoadSkeleton(const std::wstring& fileName);
    Animation* LoadAnimation(const std::wstring& fileName);

	PointLightData* AllocateLight();
	void FreeLight(PointLightData* pLight);
	void SetAmbientLight(const Vector3& color);
	const Vector3& GetAmbientLight() const;

    Physics mPhysics;
    
private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
    
	Graphics* mGraphics;
	VertexBuffer* mVertBuff;
	Camera* mCamera;
    LightingData mLightingData;
    ID3D11Buffer* mLightingBuffer;
	AssetCache<Shader>* mShaderCache;
	AssetCache<Texture>* mTextureCache;
	AssetCache<Mesh>* mMeshCache;
    AssetCache<Skeleton>* mSkeletonCache;
    AssetCache<Animation>* mAnimationCache;
	std::vector<RenderObj*> mRenderObj;
    
	bool LoadLevel(const WCHAR* fileName);
};

