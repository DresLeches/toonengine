#pragma once
#include <string>
#include <vector>
#include "./Allocators/poolAllocator.h"
#include "GameDebug.h"
#include <stdio.h>
#include <cwchar>

// Game's asset content system that holds the properties of
// each assets

class Game;
class Shader;
class Mesh;
class Texture;

template<class T> class AssetCache {
public:

	AssetCache(Game* game);
	~AssetCache();

	T* Get(const std::wstring& fileName);
    T* Load(const std::wstring& fileName);
    T* Cache(const std::wstring& key, T* asset);
    void Clear();
    
private:
    struct Pair {
		wchar_t key[64];
        T* asset;
    };

    const int tableSize = 64;

    Game* mGame;
    PoolAllocator<Pair> mPool;
    std::vector<Pair*> mTable;

    int hash(const std::wstring& fileName);
    T* findAsset(const std::wstring& fileName);
};

template<class T>
AssetCache<T>::AssetCache(Game* game)
:mGame(game)
{
    mPool.Initialize(tableSize);
	mTable.resize(tableSize);
}

template<class T>
AssetCache<T>::~AssetCache() {
	Clear();
}

template<class T>
int AssetCache<T>::hash(const std::wstring& fileName) {
    // TODO: Write a better hash function...hopefully in the near future
    if (fileName.empty())
        return -1;     // Indicate an error with fileName

    int val = 0;
    const size_t strLen = fileName.size(); 
    for (size_t i = 0; i < strLen; i++) {
        val += (int) (fileName[i]);
    }
    return (val & 0x7fffffff) % tableSize;
}

template<class T>
T* AssetCache<T>::findAsset(const std::wstring& fileName) {
    int ind = hash(fileName);
    if (ind == -1 || mTable[ind] == nullptr) {
        return nullptr;
    }
    else {
        Pair* p = mTable[ind];
        T* res = nullptr;
        for (int i = 0; i < 16; i++) {
            if (wcscmp(p[i].key, fileName.c_str()) == 0) {
                res = p[i].asset;
				break;
            }
        }
        return res;    
    }
    
}

template<class T>
T* AssetCache<T>::Get(const std::wstring& fileName) {
    return findAsset(fileName);
}

template<class T>
T* AssetCache<T>::Load(const std::wstring& fileName) {
    T* asset = findAsset(fileName);
    if (asset != nullptr) {
        return asset;
    }

    asset = T::StaticLoad(fileName.c_str(), mGame);
    if (asset != nullptr) {
        int ind = hash(fileName);
		if (ind == -1) {
			return nullptr;
		}

        // TODO: Test the number of hash collision
#if HASH_COLL
        if (mTable[ind] != nullptr) {
            printf("COLLISION DETECTED IN LOAD AT INDEX: %d", hash);
            printf("FILENAME %s CAUSED COLLISION", fileName.c_str());
        }
#endif

        if (mTable[ind] == nullptr) {

			// Try to dynamically allocate this
            Pair* block = mPool.Allocate();
			DbgAssert(block != nullptr, "Ran out of PoolBlock!");
			Pair p;
			wcscpy_s(p.key, fileName.c_str());
			p.asset = asset;
			block[0] = p;
			mTable[ind] = block;
        }
        else {
            Pair* temp = mTable[ind];
            int i;
            for (i = 0; i < 16; i++) {
                if (temp[i].asset == nullptr) {
                    break;
                }
            }
			Pair p;
			wcscpy_s(p.key, fileName.c_str());
			p.asset = asset;
			temp[i] = p;
			mTable[ind] = temp;
        }
    }
    return asset;
}

template <class T>
T* AssetCache<T>::Cache(const std::wstring& key, T* asset) {
    int ind = hash(key);
	if (ind == -1) {
		return nullptr;
	}

    // TODO: Test the number of hash collision
#if HASH_COLL
    if (mTable[ind] != nullptr) {
        printf("COLLISION DETECTED IN CACHE AT INDEX: %d", hash);
        printf("FILENAME %s CAUSED COLLISION", fileName.c_str());
    }
#endif   
    if (mTable[ind] == nullptr) {
        Pair* block = mPool.Allocate();
		DbgAssert(block != nullptr, "Ran out of PoolBlock!");
		Pair p;
		wcscpy_s(p.key, key.c_str());
		p.asset = asset;
		block[0] = p;
		mTable[ind] = block;
		return block[0].asset;
    }
    else {
        Pair* temp = mTable[ind];
        int i;
        for (i = 0; i < 16; i++) {
            if (temp[i].asset == nullptr) {
                break;
            }
        }
		Pair p;
		wcscpy_s(p.key, key.c_str());
		p.asset = asset;
		temp[i] = p;
		mTable[ind] = temp;
		return temp[i].asset;
    }
}

template<class T>
void AssetCache<T>::Clear() {
	for (Pair* p : mTable) {
		for (int i = 0; p != nullptr && i < 16; i++) {
			delete p[i].asset;
		}
	}
	mPool.Deinitialize();
    mTable.clear();
}


