#pragma once

template <int SIZE>
struct PoolAllocator {

    // The blocks are 4 bytes
    union PoolBlock
    {
        char address[4096];
        PoolBlock* pNext;
    };
    PoolBlock memPool[SIZE];
    PoolBlock* first;

    PoolAllocator();
    ~PoolAllocator() {}
    char* Allocate();
    void FreeBlock(PoolBlock* pBlock);
};

template<int SIZE>
PoolAllocator::PoolAllocator() {
    for (int i = 0; i < SIZE-1; i++) {
        memPool[i].pNext = &memPool[i];
    }
    memPool[SIZE-1] = nullptr;
    first = &memPool[0];
}

template<int SIZE>
char* PoolAllocator::Allocate() {
    if (first == nullptr) {
        return nullptr;
    }
    PoolBlock* temp = first->pNext;
    first->pNext = nullptr;
    first = temp;
    char* freeBlock = reinterpret_cast<char*>(first);
    return freeBlock;
}

template<int SIZE>
void PoolAllocator::FreeBlock(PoolBlock* pBlock) {
    PoolBlock* temp = first->pNext;
    first = pBlock;
    first->pNext = temp;
}
