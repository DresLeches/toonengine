#include "stdafx.h"
#include "engineMath.h"
#include "UnitTest/PhysicsTest.h"

int main()
{
	bool result = PhysicsUnitTest::Test_AABB_AABB_Intersection();
	DbgAssert(result == true, "AABB-AABB Intersection Test Failed\n");
	result = PhysicsUnitTest::Test_RayCast_ABB_Intersection();
	DbgAssert(result == true, "RayCast-AABB Intersection Test Failed\n");
}
